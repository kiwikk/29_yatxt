package com.gitlab.yatxt.trigger;

import android.content.Context;

import java.util.HashMap;

public class TriggerFactory {
    private static final String SAMPLE_TRIGGER_ID = "sample";
    private static final String WEATHER_TRIGGER_ID = "weather";
    private static final String ACCELEROMETER_TRIGGER_ID = "accelerometer";
    private static final String LIGHT_TRIGGER_ID = "light";
    private static final String PRESSURE_TRIGGER_ID = "pressure";
    private static final String CURRENCY_TRIGGER_ID = "currency";
    private static final String TIME_TRIGGER_ID = "time";

    private static final HashMap<String, TriggerAllocator> triggers = new HashMap<>();
    private final Context context;

    public TriggerFactory(Context context) {
        this.context = context;
    }

    static {
        triggers.put(SAMPLE_TRIGGER_ID, new TriggerAllocator() {
            @Override
            public Trigger allocate(Context context) {
                return new SampleTrigger(context);
            }
        });
        triggers.put(WEATHER_TRIGGER_ID, new TriggerAllocator() {
            @Override
            public Trigger allocate(Context context) {
                return new WeatherTrigger(context);
            }
        });
        triggers.put(ACCELEROMETER_TRIGGER_ID, new TriggerAllocator() {
            @Override
            public Trigger allocate(Context context) {
                return new AccelerometerTrigger(context);
            }
        });
        triggers.put(LIGHT_TRIGGER_ID, new TriggerAllocator() {
            @Override
            public Trigger allocate(Context context) {
                return new LightTrigger(context);
            }
        });
        triggers.put(PRESSURE_TRIGGER_ID, new TriggerAllocator() {
            @Override
            public Trigger allocate(Context context) {
                return new PressureTrigger(context);
            }
        });
        triggers.put(CURRENCY_TRIGGER_ID, new TriggerAllocator() {
            @Override
            public Trigger allocate(Context context) {
                return new CurrencyTrigger(context);
            }
        });
        triggers.put(TIME_TRIGGER_ID, new TriggerAllocator() {
            @Override
            public Trigger allocate(Context context) {
                return new TimeTrigger(context);
            }
        });
    }

    public Trigger newInstance(String id) throws InstantiationException {
        TriggerAllocator allocator = triggers.get(id);
        if (allocator == null) {
            throw new InstantiationException(String.format("no trigger matching id=%s", id));
        }
        return allocator.allocate(context);
    }

    private interface TriggerAllocator {
        Trigger allocate(Context context);
    }
}
