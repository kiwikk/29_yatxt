package com.gitlab.yatxt.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.style.LeadingMarginSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.yatxt.book.Book;
import com.gitlab.yatxt.book.BookMark;
import com.gitlab.yatxt.utils.FileLocation;
import com.gitlab.yatxt.pagesplitter.LayoutCache;
import com.gitlab.yatxt.pagesplitter.LayoutSupplier;
import com.gitlab.yatxt.ui.views.OverlayDecorator;
import com.gitlab.yatxt.book.ProgressInfo;
import com.gitlab.yatxt.pagesplitter.PuzzleChunkAdapter;
import com.gitlab.yatxt.pagesplitter.PuzzleChunkSupplier;
import com.gitlab.yatxt.R;
import com.gitlab.yatxt.book.SectionTracker;
import com.gitlab.yatxt.trigger.Trigger;
import com.gitlab.yatxt.trigger.TriggerFactory;
import com.gitlab.yatxt.utils.IOUtils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.gitlab.yatxt.YaTxtApplication.TAG;
import static com.gitlab.yatxt.pagesplitter.PuzzleChunkSupplier.PuzzleChunk;
import static com.gitlab.yatxt.book.SectionTracker.Section;

public class ReadActivity extends AppCompatActivity {
    public static final String BOOK_PATH_MESSAGE = "com.gitlab.yatxt.BOOK_PATH_MESSAGE";

    public static final int SUCCESS = 0;
    public static final int FAILURE = 1;

    public static final float FONT_SIZE = 16f; // sp units
    public static final int FONT_COLOR = Color.BLACK;
    public static final float PADDING_LEFT = 20f; // dp units
    public static final float PADDING_RIGHT = 20f; // dp units
    public static final float SPACING_ADD = 7f; // sp units
    public static final float SPACING_MULT = 1f;

    private static final String TEMP_DIR_NAME = "temp";
    private static final String PROGRESS_DIR_NAME = "progress";
    private static final String PROGRESS_INFO_FILE_NAME = "progress-info.xml";
    private static final String BRANCH_FILE_NAMING_PATTERN = "[0-9]+_branch.json";

    private Book book;
    private SectionTracker sectionTracker;
    private ProgressInfo progressInfo;
    private PuzzleChunkSupplier pager;
    private RecyclerView recyclerView;
    private DataLoadersNotifier dataLoadersNotifier;
    private BottomHitNotifier bottomHitNotifier;
    private PuzzleChunkAdapter adapter;
    private LayoutSupplier layoutSupplier;
    private final ArrayList<PuzzleChunk> chunks = new ArrayList<>();
    private File infoFile;
    private File branchFile;
    private File bookProgressDir;

    private final ExecutorService worker = Executors.newFixedThreadPool(1);

    public static void startReadActivity(Context context, String bookPath) {
        Intent intent = new Intent(context, ReadActivity.class);
        intent.putExtra(ReadActivity.BOOK_PATH_MESSAGE, bookPath);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_read_activity);
        getSupportActionBar().hide();
        if (savedInstanceState == null) {
            final String receivedBookPath = getIntent().getStringExtra(BOOK_PATH_MESSAGE);
            if (receivedBookPath == null) {
                // report error
            } else {
                // show progress bar
                worker.submit(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            initReadEnvironment(receivedBookPath);
                            loadBook();
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    initView();
                                    recyclerView.scrollToPosition(Math.max(0, chunks.size() - 2));
                                }
                            });
                        } catch (IOException exc) {
                            Log.e(TAG, "failed to init read session cause: " + exc.getMessage());
                        }
                    }
                });
            }
        } else {
            // restore state
        }
    }

    private void initReadEnvironment(String receivedBookPath) {
        try {
            this.book = Book.fromRootDir(receivedBookPath);
            File tempDir = new File(getFilesDir(), TEMP_DIR_NAME);
            if (IOUtils.forceMkdirIfNotExists(tempDir)) {
                Log.d(TAG, "temp directory created at: " + tempDir.getAbsolutePath());
            }
            String tempFileName = String.format("%s_temp.xml", book.getId());
            File tempFile = new File(tempDir, tempFileName);
            IOUtils.deleteIfExists(tempFile);
            IOUtils.forceCreate(tempFile);
            Log.d(TAG, "created temp file at: " + tempFile.getAbsolutePath());

            File progressDir = new File(getFilesDir(), PROGRESS_DIR_NAME);
            if (IOUtils.forceMkdirIfNotExists(progressDir)) {
                Log.d(TAG, "progress directory created at: " + progressDir.getAbsolutePath());
            }
            String bookProgressDirName = String.format("%s_progress", book.getId());
            bookProgressDir = new File(progressDir, bookProgressDirName);
            if (IOUtils.forceMkdirIfNotExists(bookProgressDir)) {
                Log.d(TAG, "book progress directory created at: " + bookProgressDir.getAbsolutePath());
            }

            infoFile = new File(bookProgressDir, PROGRESS_INFO_FILE_NAME);
            if (IOUtils.dirEmpty(bookProgressDir)) {
                onProgressDirEmpty(book, bookProgressDir);
            }

            if (!IOUtils.fileExists(infoFile)) {
                onProgressInfoFileMissed(book, bookProgressDir);
            }

            ProgressInfo pi = ProgressInfo.fromFile(infoFile);
            if (pi.getBranchId() == null) {
                onActiveBranchInfoMissed(book, bookProgressDir);
                pi = ProgressInfo.fromFile(infoFile);
            }

            branchFile = new File(bookProgressDir, pi.getBranchId() + ".json");
            if (!IOUtils.fileExists(new File(bookProgressDir, pi.getBranchId() + ".json"))) {
                onActiveBranchInfoInvalid(book, bookProgressDir);
            }

            this.progressInfo = ProgressInfo.fromFile(infoFile);
            this.sectionTracker = SectionTracker.fromFile(
                    book, new TriggerFactory(this),
                    branchFile);

            TextPaint mockPaint = new TextPaint();
            mockPaint.setTextSize(spToPx(FONT_SIZE));
            mockPaint.setColor(FONT_COLOR);
            mockPaint.setAntiAlias(true);
            mockPaint.setTypeface(Typeface.createFromAsset(getAssets(), "font/roboto_regular.ttf"));

            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            StaticLayout mockLayout = new StaticLayout("\n",
                    mockPaint,
                    dm.widthPixels - dpToPx(PADDING_LEFT) - dpToPx(PADDING_RIGHT),
                    Layout.Alignment.ALIGN_NORMAL,
                    SPACING_MULT, spToPx(SPACING_ADD), true
            );

            LayoutCache.get().clear();
            this.layoutSupplier = new LayoutSupplier(mockLayout);
            this.pager = new PuzzleChunkSupplier(tempFile, evaluateChunkSize(mockLayout), layoutSupplier);
        } catch (Exception exc) {
            Log.e(TAG, ((exc.getMessage() == null) ? "" : exc.getMessage()));
            exc.printStackTrace();
            setResult(FAILURE);
        }
    }

    private void initView() {
        setContentView(R.layout.layout_read_activity);
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        this.adapter = new PuzzleChunkAdapter(chunks) {
            @Override
            public void onLayoutRequested(int position) {
                worker.submit(new LoadLayoutTask(position) {
                    @Override
                    public void onLayoutLoaded() {
                        recyclerView.getAdapter().notifyItemChanged(position);
                    }

                    @Override
                    public void onLayoutLoadingFailed(Exception cause) {
                        Log.e(TAG, "failed while loading layout cause: " + cause.getMessage());
                        cause.printStackTrace();
                    }
                });
            }
        };

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int bottomPadding = dm.heightPixels / 6;
        recyclerView.setPadding(0, 0, 0, bottomPadding);
        recyclerView.setClipToPadding(false);

        recyclerView.addItemDecoration(new OverlayDecorator(-spToPx(FONT_SIZE + SPACING_ADD)));
//        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        this.dataLoadersNotifier = new DataLoadersNotifier(layoutManager);
        dataLoadersNotifier.addOnLoadMoreListener(new DataLoadersNotifier.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                int preload = 1;
                worker.submit(new LoadChunksTask(preload) {
                    @Override
                    public void onChunksLoaded(int pos, int loaded) {
                        if (loaded > 0) {
                            adapter.notifyItemRangeInserted(pos, loaded);
                        } else {
                            onChunksEnd();
                        }
                    }

                    @Override
                    public void onChunksLoadingFailed(Exception cause) {
                        Log.e(TAG, "failed while loading chunk cause" + cause.getMessage());
                        cause.printStackTrace();
                    }
                });
            }
        });
        this.bottomHitNotifier = new BottomHitNotifier();
        recyclerView.addOnScrollListener(dataLoadersNotifier);
        recyclerView.addOnScrollListener(bottomHitNotifier);
        recyclerView.invalidate();
    }

    private void loadBook() throws IOException {
        final int ENTIRE_SECTION = 0;
        final int UNTIL_BOOKMARK = 1;
        final int SKIP = 2;

        BookMark bm = progressInfo.getBookMark();
        int mode = (sectionTracker.getSection(0).getId().
                equals(bm.getSectionId())) ? UNTIL_BOOKMARK : ENTIRE_SECTION;

        for (int i = 0; i < sectionTracker.getSize(); ++i) {
            Section sec = sectionTracker.getSection(i);
            FileLocation textLoc = sec.getTextLocation();
            pager.attachFile(book.resolveSectionPath(sec.getId()), textLoc.getOff(), textLoc.getLen());

            if (sec.getId().equals(bm.getSectionId())) {
                mode = UNTIL_BOOKMARK;
            }

            PuzzleChunk chunk;
            switch (mode) {
                case ENTIRE_SECTION:
                    for (; (chunk = pager.requestNextChunk()) != null; ) {
                        chunks.add(chunk);
                    }
                    break;
                case UNTIL_BOOKMARK:
                    for (; (chunk = pager.requestNextChunk()) != null; ) {
                        chunks.add(chunk);
                        if (chunk.getStart() > bm.getLocation()) {
                            break;
                        }
                    }
                    mode = SKIP;
                    break;
            }
        }
    }

    private void onProgressDirEmpty(Book book, File progressDir) throws Exception {
        try {
            initBookProgressDir(book, progressDir);
            Log.d(TAG, "book progress directory initialized");
        } catch (Exception exc) {
            Log.e(TAG, "failed while init empty progress directory");
            throw exc;
        }
    }

    private void onProgressInfoFileMissed(Book book, File progressDir) throws Exception {
        onActiveBranchInfoMissed(book, progressDir);
    }

    private void onActiveBranchInfoInvalid(Book book, File progressDir) throws Exception {
        onActiveBranchInfoMissed(book, progressDir);
    }

    private void onActiveBranchInfoMissed(Book book, File progressDir) throws Exception {
        List<String> branches = listBranchFileNames(progressDir);
        if (branches.isEmpty()) {
            initBookProgressDir(book, progressDir);
        } else {
            String branchId = FilenameUtils.removeExtension(branches.get(branches.size() - 1));
            ProgressInfo progressInfo = new ProgressInfo(
                    branchId,
                    new BookMark(book.getBootSectionId(), 0)
            );
            File progInfoFile = new File(progressDir, PROGRESS_INFO_FILE_NAME);
            try {
                replaceXMLProgressInfo(progInfoFile, progressInfo);
            } catch (IOException exc) {
                Log.e(TAG, "failed while repairing missed active branch info");
                throw exc;
            }
        }
    }

    private void onChunksEnd() {
        final Trigger trigger = sectionTracker.getLastSection().getTrigger();
        if (trigger == null) {
            onBookEnd();
        } else {
            trigger.addOnTriggerEvaluatedListener(new Trigger.OnTriggerEvaluatedListener() {
                @Override
                public void onEvaluated(final String result) {
                    Trigger.Switch sw = trigger.firstMatchingSwitch(result);

                    Log.d(TAG, String.format("[%s] returned result [%s]", trigger.getClass().getSimpleName(), result));
                    Log.d(TAG, String.format("switch [%s] was caught", sw.toString()));

                    try {
                        worker.submit(new LoadSectionTask(sw.getSectionId(), 1) {

                            @Override
                            public void onSectionLoaded(final int pos, final int chunksPreloaded) {
                                adapter.addAnimatedItem(chunks.get(pos),
                                        AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up_animation));
                                if (!recyclerView.canScrollVertically(1)) {
                                    adapter.notifyItemRangeInserted(pos, chunksPreloaded);
                                } else {
                                    bottomHitNotifier.addOnBottomHitListener(new BottomHitNotifier.OnBottomHitListener() {
                                        @Override
                                        public void onBottomHit() {
                                            adapter.notifyItemRangeInserted(pos, chunksPreloaded);
                                            bottomHitNotifier.clearListeners();
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onSectionLoadingFailed(Exception cause) {
                                Log.e(TAG, "failed while loading section cause: " + cause.getMessage());
                                cause.printStackTrace();
                            }
                        });
                    } catch (Exception exc) {
                        Log.e(TAG, "failed while loading new section: " + exc.getMessage());
                    }
                }
            });
            Log.d(TAG, String.format("[%s] was requested to evaluation", trigger.getClass().getSimpleName()));
            trigger.evaluate();
        }
    }

    private abstract class LoadLayoutTask implements Runnable {
        private static final int SUCCESS = 0;
        private static final int FAILURE = 1;

        Handler handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.what == SUCCESS) {
                    onLayoutLoaded();
                } else if (msg.what == FAILURE) {
                    onLayoutLoadingFailed((Exception) msg.obj);
                }
                return false;
            }
        });
        final int position;

        LoadLayoutTask(int position) {
            this.position = position;
        }

        @Override
        public void run() {
            try {
                loadLayout(position);
                handler.sendEmptyMessage(SUCCESS);
            } catch (Exception exc) {
                Log.d(TAG, "failed while loading layout");
                handler.sendMessage(Message.obtain(handler, FAILURE, exc));
            }
        }

        public abstract void onLayoutLoaded();

        public abstract void onLayoutLoadingFailed(Exception cause);
    }

    private abstract class LoadChunksTask implements Runnable {
        private static final int SUCCESS = 0;
        private static final int FAILURE = 1;

        private static final String POS = "pos";
        private static final String LEN = "len";

        private final int n;
        private final Bundle data = new Bundle();

        private final Handler handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.what == SUCCESS) {
                    Bundle data = msg.getData();
                    int pos = (Integer) data.get(POS);
                    int len = (Integer) data.get(LEN);
                    onChunksLoaded(pos, len);
                } else if (msg.what == FAILURE) {
                    onChunksLoadingFailed((Exception) msg.obj);
                }
                return false;
            }
        });

        LoadChunksTask(int n) {
            this.n = n;
        }

        @Override
        public final void run() {
            try {
                int pos;
                synchronized (chunks) {
                    pos = chunks.size();
                }

                int loaded = loadChunks(n);

                data.clear();
                data.putInt(POS, pos);
                data.putInt(LEN, loaded);

                Message msg = new Message();
                msg.setData(data);
                msg.what = SUCCESS;
                handler.sendMessage(msg);

            } catch (Exception exc) {
                Log.e(TAG, "failed to load new chunk cause: " + exc.getMessage());
                handler.sendMessage(Message.obtain(handler, FAILURE, exc));
            }
        }

        public abstract void onChunksLoaded(int pos, int loaded);

        public abstract void onChunksLoadingFailed(Exception cause);
    }

    private abstract class LoadSectionTask implements Runnable {
        private static final int SUCCESS = 0;
        private static final int FAILURE = 1;

        private static final String POS = "pos";
        private static final String LEN = "len";

        private final String sectionId;
        private final int preload;
        private final Bundle data = new Bundle();

        Handler handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                if (msg.what == SUCCESS) {
                    Bundle bundle = msg.getData();
                    onSectionLoaded(bundle.getInt(POS), bundle.getInt(LEN));
                } else if (msg.what == FAILURE) {
                    onSectionLoadingFailed((Exception) msg.obj);
                }
                return false;
            }
        });

        public LoadSectionTask(String sectionId, int preload) {
            this.sectionId = sectionId;
            this.preload = preload;
        }

        @Override
        public void run() {
            try {
                int pos;
                synchronized (chunks) {
                    pos = chunks.size();
                }
                int loaded = loadSection(sectionId, preload);

                data.clear();
                data.putInt(POS, pos);
                data.putInt(LEN, loaded);
                Message msg = new Message();
                msg.setData(data);
                msg.what = SUCCESS;
                handler.sendMessage(msg);

            } catch (Exception exc) {
                handler.sendMessage(Message.obtain(handler, FAILURE, exc));
            }
        }

        public abstract void onSectionLoaded(int pos, int chunksPreloaded);

        public abstract void onSectionLoadingFailed(Exception cause);
    }

    private void loadLayout(int position) throws Exception {
        if (position < 0 || position >= chunks.size()) {
            throw new RuntimeException("attempt to load layout for non existing chunk");
        }
        PuzzleChunk chunk;
        synchronized (chunks) {
            chunk = chunks.get(position);
        }
        CharSequence raw = pager.loadChunk(chunk);
        SpannableStringBuilder spanText = new SpannableStringBuilder();
        if (chunk.getIndentWidth() == 0) {
            spanText.append("\n");
        }
        spanText.append(raw);
        spanText.setSpan(new LeadingMarginSpan.Standard(
                (int) chunk.getIndentWidth(), 0), 0, raw.length(), 0);
        StaticLayout layout = layoutSupplier.obtain(spanText);
        LayoutCache.get().put(chunk, layout);
    }

    private int loadChunks(int n) throws IOException {
        int loaded = 0;
        synchronized (chunks) {
            for (; loaded < n; ) {
                PuzzleChunk chunk = pager.requestNextChunk();
                if (chunk == null) {
                    break;
                } else {
                    chunks.add(chunk);
                    ++loaded;
                }
            }
        }
        return loaded;
    }

    private int loadSection(String sectionId, int preload) throws Exception {
        Section sec = sectionTracker.addSection(sectionId);
        pager.attachFile(book.resolveSectionPath(sectionId), sec.getTextLocation());
        return loadChunks(preload);
    }

    private static class DataLoadersNotifier extends RecyclerView.OnScrollListener {
        private int visibleThreshold = 1;
        private int currentPage = 0;
        private int previousTotalItemCount = 0;
        private boolean loading = true;
        private int startingPageIndex = 0;
        private final LinkedList<OnLoadMoreListener> listeners = new LinkedList<>();

        LinearLayoutManager mLayoutManager;

        DataLoadersNotifier(LinearLayoutManager layoutManager) {
            this.mLayoutManager = layoutManager;
        }

        @Override
        public void onScrolled(@NonNull RecyclerView view, int dx, int dy) {
            int lastVisibleItemPosition = 0;
            int totalItemCount = mLayoutManager.getItemCount();

            lastVisibleItemPosition = mLayoutManager.findLastVisibleItemPosition();

            if (totalItemCount < previousTotalItemCount) {
                this.currentPage = this.startingPageIndex;
                this.previousTotalItemCount = totalItemCount;
                if (totalItemCount == 0) {
                    this.loading = true;
                }
            }
            if (loading && (totalItemCount > previousTotalItemCount)) {
                loading = false;
                previousTotalItemCount = totalItemCount;
            }

            if (!loading && (lastVisibleItemPosition + visibleThreshold) >= totalItemCount) {
                currentPage++;
                notifyListeners();
                loading = true;
            }
        }

        private void notifyListeners() {
            for (OnLoadMoreListener listener : listeners) {
                listener.onLoadMore();
            }
        }

        public void addOnLoadMoreListener(@NonNull OnLoadMoreListener listener) {
            listeners.add(listener);
        }

        public void clearListeners() {
            listeners.clear();
        }

        interface OnLoadMoreListener {
            void onLoadMore();
        }

        public void resetState() {
            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = 0;
            this.loading = true;
        }
    }

    private static class BottomHitNotifier extends RecyclerView.OnScrollListener {
        private final LinkedList<OnBottomHitListener> listeners = new LinkedList<>();

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            if (!recyclerView.canScrollVertically(1)) {
                notifyAllListeners();
            }
        }

        private void notifyAllListeners() {
            for (OnBottomHitListener listener : listeners) {
                listener.onBottomHit();
            }
        }

        public void addOnBottomHitListener(OnBottomHitListener listener) {
            listeners.add(listener);
        }

        public void clearListeners() {
            listeners.clear();
        }

        public interface OnBottomHitListener {
            void onBottomHit();
        }
    }

    private void onBookEnd() {
        Log.d(TAG, "book hit end");
        int branchNum = Integer.parseInt(progressInfo.getBranchId().split("_")[0]);
        String newBranchId = String.format("%d_branch", branchNum + 1);
        File newBranch = new File(bookProgressDir, newBranchId + ".json");
        LinkedList<String> ids = new LinkedList<>();
        ids.add(book.getBootSectionId());
        BookMark bm = new BookMark(book.getBootSectionId(), 0);
        progressInfo = new ProgressInfo(newBranchId, bm);
        try {
            replaceJSONBranch(newBranch, ids);
            Log.d(TAG, String.format("created new branch %s", newBranchId));
            replaceXMLProgressInfo(infoFile, progressInfo);
            Log.d(TAG, String.format("switched to new branch %s", newBranchId));
        } catch (Exception ignore) {
            Log.d(TAG, String.format("unable to create new branch %s", newBranch.getAbsolutePath()));
        }
    }

    /**
     * @return List branchIds sorted in lexicographic order.
     */
    private List<String> listBranchFileNames(File progressDir) {
        String[] branchIds = progressDir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.matches(BRANCH_FILE_NAMING_PATTERN);
            }
        });
        if (branchIds == null) {
            branchIds = new String[0];
        }
        Arrays.sort(branchIds);
        return new LinkedList<>(Arrays.asList(branchIds));
    }

    /**
     * Create info file and initial branch in specified directory.
     */
    private void initBookProgressDir(Book book, File dir) throws Exception {
        File info = new File(dir, PROGRESS_INFO_FILE_NAME);
        File branch = new File(dir, getJSONBranchFileName(0));
        ProgressInfo progressInfo = new ProgressInfo(
                FilenameUtils.removeExtension(branch.getName()),
                new BookMark(book.getBootSectionId(), 0));

        replaceXMLProgressInfo(info, progressInfo);
        Log.d(TAG, "progress-info file created at: " + info.getAbsolutePath());
        replaceJSONBranch(branch, Collections.singletonList(book.getBootSectionId()));
        Log.d(TAG, "branch file created at: " + branch.getAbsolutePath());
    }

    private String getJSONBranchFileName(int id) {
        return String.format(Locale.US, "%d_branch.json", id);
    }

    private void replaceXMLProgressInfo(File progressInfoFile, ProgressInfo info) throws IOException {
        IOUtils.deleteIfExists(progressInfoFile);
        String str = info.toXmlString();
        FileUtils.writeStringToFile(progressInfoFile, info.toXmlString(), StandardCharsets.UTF_8);
    }

    private void replaceJSONBranch(File branch, List<String> sectionIds) throws Exception {
        JSONArray arr = new JSONArray();
        for (String id : sectionIds) {
            arr.put(id);
        }
        JSONObject obj = new JSONObject();
        try {
            obj.put(SectionTracker.JSON_SECTION_ARRAY_FIELD, arr);
            FileUtils.writeStringToFile(branch, obj.toString(), StandardCharsets.UTF_8);
        } catch (JSONException exc) {
            Log.e(TAG, "failed while creating json object");
            throw exc;
        } catch (IOException exc) {
            Log.e(TAG, "failed while writing data to branch file");
            throw exc;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressInfo != null) {
            int lastChunkIndex = Math.max(0, chunks.size() - 2);
            PuzzleChunk lastChunk = chunks.get(lastChunkIndex);
            int location = lastChunk.getStart();
            String sectionId = sectionTracker.getLastSection().getId();
            BookMark newBookMark = new BookMark(sectionId, location);
            ProgressInfo newProgressInfo = new ProgressInfo(progressInfo.getBranchId(), newBookMark);
            try {
                replaceXMLProgressInfo(infoFile, newProgressInfo);
                replaceJSONBranch(branchFile, sectionTracker.getSectionIds());
            } catch (Exception ignore) {
                Log.d(TAG, "failed to save progress info");
            }
        }
    }

    private int dpToPx(float dp) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                getResources().getDisplayMetrics()
        );
    }

    private int spToPx(float sp) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP,
                sp,
                getResources().getDisplayMetrics()
        );
    }

    private float pxToDp(int px) {
        DisplayMetrics dp = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dp);
        return (float) Math.ceil(dp.density * px);
    }

    private float pxToSp(int px) {
        DisplayMetrics dp = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dp);
        return (float) Math.ceil(dp.scaledDensity * px);
    }

    private int evaluateChunkSize(StaticLayout mockLayout) {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int frameWidth = mockLayout.getWidth();
        int frameHeight = dm.heightPixels;
        TextPaint paint = mockLayout.getPaint();
        int fontWidth = (int) (paint.getTextScaleX() * paint.getTextSize());
        int fontHeight = (int) paint.getTextSize();

        int charsPerFrame = (int) ((frameHeight / (float) fontHeight) * (frameWidth / (float) fontWidth));
        final int framesPerPage = 2;
        return charsPerFrame * framesPerPage;
    }
}
