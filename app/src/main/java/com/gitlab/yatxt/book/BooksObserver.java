package com.gitlab.yatxt.book;

import android.os.Environment;
import android.util.Log;

import com.gitlab.yatxt.YaTxtApplication;
import com.gitlab.yatxt.utils.IOUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import static com.gitlab.yatxt.YaTxtApplication.TAG;

public class BooksObserver {
    private static final BooksObserver singleton = new BooksObserver();
    private final HashMap<String, Book> library = new HashMap<>();

    private BooksObserver() {
        refresh();
    }

    public Book getBookById(String bookId) {
        return library.get(bookId);
    }

    private void collectBooksInfo() {
        File appDir = YaTxtApplication.getAppDir();
        if (!IOUtils.dirExists(appDir)) {
            Log.d(TAG, String.format("cannot find directory %s", appDir.getAbsolutePath()));
            return;
        }

        File[] files = appDir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });
        if (files == null) {
            Log.d(TAG, String.format("unable to access files in directory %s", appDir.getAbsolutePath()));
            return;
        }
        for (File file : files) {
            try {
                Book book = Book.fromRootDir(file.getAbsolutePath());
                library.put(book.getId(), book);
            } catch (Exception e) {
                Log.d(TAG, String.format("cannot read metainfo from %s", file.getAbsolutePath()));
            }
        }
    }

    public void refresh() {
        collectBooksInfo();
    }

    public static BooksObserver getInstance() {
        return singleton;
    }
}
