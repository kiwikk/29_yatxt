package com.gitlab.yatxt.ui.library;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.yatxt.R;
import com.gitlab.yatxt.book.Book;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LibraryFragment extends Fragment {

    private LibraryViewModel libraryViewModel;
    private static final String URL = "https://kiwikk.pythonanywhere.com/";
    boolean isFinished = false;

    ImageView imageView;
    // public String url = "https://kiwikk.pythonanywhere.com/cover?name=book1";
    //public String url1 = "https://kiwikk.pythonanywhere.com/cover?name=book2";

    LibraryAdapter libraryAdapter;
    RecyclerView recyclerView;

    String[] result;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        libraryViewModel =
                ViewModelProviders.of(this).get(LibraryViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_library, container, false);

        imageView = root.findViewById(R.id.imageView);

        recyclerView = root.findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        libraryAdapter = new LibraryAdapter(getActivity(), getMyList());
        recyclerView.setAdapter(libraryAdapter);

        return root;
    }

    private ArrayList<BookModel> getMyList() {
        ArrayList<BookModel> bookModels = new ArrayList<>();

        getBookNames(URL);

        while (!isFinished) {
        }

        String[] book_names = result[0].split(" ");

        for (int i = 0; i < book_names.length; ++i) {
            Book book = new Book();
            book.getInputStream(book_names[i]);

            while (!book.isFinished()) {
            }
            book.parseDescription(book.getDescriptionFromXml());

            BookModel m = new BookModel();
            m.setBookName(book_names[i]);
            m.setTitle(book.getBookName());
            m.setDescription(book.getDescription());
            m.setGenre(book.getGenre());
            m.setAuthor(book.getAuthor());
            m.setId(book.getId());
            m.setImg(URL + "cover?name=" + book_names[i]);
            bookModels.add(m);
        }

        return bookModels;
    }

    private void getBookNames(String url) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();

        result = new String[1];
        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    Log.d("OkHttpClient", "Book 61");
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code" + response);
                    else {
                        result[0] = response.body().string();
                        isFinished = true;
                    }
                }
            });
        } catch (Exception e) {
            Log.e("Exception Book 49", e.getMessage());
            e.printStackTrace();
        }
    }
}