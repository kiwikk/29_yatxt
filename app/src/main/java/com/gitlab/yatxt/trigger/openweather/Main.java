package com.gitlab.yatxt.trigger.openweather;

public class Main {
    public double temp;
    public double feels_like;
    public double temp_min;
    public double temp_max;
    public double pressure;
    public double humidity;
}
