package com.gitlab.yatxt.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

public class RandomAccessFileOutputAdapter extends OutputStream {
    private final RandomAccessFile raFile;

    public RandomAccessFileOutputAdapter(RandomAccessFile raFile) {
        this.raFile = raFile;
    }

    @Override
    public void write(int b) throws IOException {
        raFile.seek(raFile.length());
        raFile.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        raFile.seek(raFile.length());
        raFile.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        raFile.seek(raFile.length());
        raFile.write(b, off, len);
    }
}
