package com.gitlab.yatxt.ui.find;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.yatxt.R;
import com.gitlab.yatxt.book.Book;
import com.gitlab.yatxt.ui.library.BookModel;
import com.gitlab.yatxt.ui.library.LibraryAdapter;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FindFragment extends Fragment {

    private static final String URL = "https://kiwikk.pythonanywhere.com/";
    private FindViewModel findViewModel;
    LibraryAdapter adapter;
    String[] result;
    boolean isFinished;
    private SearchView searchView = null;
    RecyclerView recyclerView;
    private SearchView.OnQueryTextListener queryTextListener;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        findViewModel =
                ViewModelProviders.of(this).get(FindViewModel.class);
        View root = inflater.inflate(R.layout.fragment_find, container, false);

        recyclerView = root.findViewById(R.id.search_recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new LibraryAdapter(getActivity(), getMyList());
        recyclerView.setAdapter(adapter);


        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private ArrayList<BookModel> getMyList() {
        ArrayList<BookModel> bookModels = new ArrayList<>();

        getBookNames(URL);

        while (!isFinished) {
        }

        String[] book_names = result[0].split(" ");

        for (int i = 0; i < book_names.length; ++i) {
            Book book = new Book();
            book.getInputStream(book_names[i]);

            while (!book.isFinished()) {
            }
            book.parseDescription(book.getDescriptionFromXml());

            BookModel m = new BookModel();
            m.setTitle(book.getBookName());
            m.setDescription(book.getDescription());
            m.setGenre(book.getGenre());
            m.setAuthor(book.getAuthor());
            m.setId(book.getId());
            m.setImg(URL + "cover?name=" + book_names[i]);
            bookModels.add(m);
        }

        return bookModels;
    }

    private void getBookNames(String url) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();

        result = new String[1];
        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    Log.d("OkHttpClient", "Book 61");
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code" + response);
                    else {
                        result[0] = response.body().string();
                        isFinished = true;
                    }
                }
            });
        } catch (Exception e) {
            Log.e("Exception Book 49", e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.find_menu, menu);
        MenuItem item = menu.findItem(R.id.search_recycle);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (item != null) {
            searchView = (SearchView) item.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);
                    adapter.getFilter().filter(newText);
                    return true;
                }
            };

            searchView.setOnQueryTextListener(queryTextListener);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.search_item:
//                // Not implemented here
//                return false;
//            default:
//                break;
//        }
//        searchView.setOnQueryTextListener(queryTextListener);
//        return super.onOptionsItemSelected(item);
//    }

}