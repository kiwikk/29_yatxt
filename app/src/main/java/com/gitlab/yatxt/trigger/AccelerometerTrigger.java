package com.gitlab.yatxt.trigger;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import java.util.Arrays;

public class AccelerometerTrigger extends Trigger {
    private SensorManager sensorManager;

    private final String accelerometerKey = "accelerometer";
    Handler handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            Bundle data = msg.getData();
            String result = data.getString(accelerometerKey);
            sendResult(result);
            return false;
        }
    });

    private void sendErrResult() {
        Bundle data = new Bundle();
        data.putString(accelerometerKey, "err");
        Message msg = new Message();
        msg.setData(data);
        handler.sendMessage(msg);
    }

    AccelerometerTrigger(Context context) {
        super(context);
    }

    @Override
    public void addSwitch(String condition, String sectionId) {
        switches.add(new AccelerometerSwitch(condition, sectionId));
    }

    @Override
    public void evaluate() {
        try {
            sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
            Sensor accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            if (accelerometerSensor == null) {
                sendErrResult();
                return;
            }

            sensorManager.registerListener(this.new AccelerometerListener(), accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        } catch (Throwable ignored) {
            sendErrResult();
        }
    }

    private class AccelerometerListener implements SensorEventListener {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            Sensor triggeredSensor = sensorEvent.sensor;

            if (triggeredSensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                sensorManager.unregisterListener(this);
                sendResult(Arrays.toString(sensorEvent.values));
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    }

    private class AccelerometerSwitch extends Trigger.Switch {
        AccelerometerSwitch(String condition, String sectionId) {
            super(condition, sectionId);
        }

        @Override
        public boolean matchCondition(String value) {
            if (condition.equals(DEFAULT_CONDITION)) {
                return true;
            }
            if (value.equals("err")) {
                return false;
            }

            try {
                String[] conditionString = condition.split(" ");
                String[] valueString = value.split(" ");
                for (int i = 0; i < 3; ++i) {
                    if (Double.parseDouble(valueString[i]) > Double.parseDouble(conditionString[i])) {
                        return false;
                    }
                }

                return true;
            } catch (Throwable ignored) {
                return false;
            }
        }
    }
}
