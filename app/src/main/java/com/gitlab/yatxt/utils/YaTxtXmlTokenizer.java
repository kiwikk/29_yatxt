package com.gitlab.yatxt.utils;

import com.gitlab.yatxt.exc.NoSuchAttributeException;
import com.gitlab.yatxt.exc.XmlParsingException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class implements light weight prefix driven xml document tokenizer.
 * This class concerned only about effective tokenization, so it doesn't perform any validations
 * and allows unacceptable xml documents.
 */
public class YaTxtXmlTokenizer {
    private final YaTxtCharInputStream charStream;
    private final List<Seeker> seekers;
    private Token token;
    private int ch;
    private int offset;
    private int offsetCh;

    public static final Token EOF = new EOF();

    private final Pattern openTagPrefixPattern = Pattern.compile("<[^\\p{Punct}]");
    private final Pattern closeTagPrefixPattern = Pattern.compile("</");
    private final Pattern commentTagPrefixPattern = Pattern.compile("<!--");
    private final Pattern cdataTagPrefixPattern = Pattern.compile("<!\\[CDATA\\[");
    private final Pattern preambleTagPrefixPattern = Pattern.compile("<\\?xml");

    public YaTxtXmlTokenizer(InputStream in) {
        this.seekers = new ArrayList<>();
        initSeekers();

        this.charStream = new YaTxtCharInputStream(in);
    }

    private void initSeekers() {
        seekers.clear();
        seekers.add(new Seeker() {
            @Override
            public boolean seek() throws IOException {
                return YaTxtXmlTokenizer.this.parseOpenTag();
            }
        });
        seekers.add(new Seeker() {
            @Override
            public boolean seek() throws IOException {
                return YaTxtXmlTokenizer.this.parseCloseTag();
            }
        });
        seekers.add(new Seeker() {
            @Override
            public boolean seek() throws IOException {
                return YaTxtXmlTokenizer.this.parseComment();
            }
        });
        seekers.add(new Seeker() {
            @Override
            public boolean seek() throws IOException {
                return YaTxtXmlTokenizer.this.parseCDATA();
            }
        });
        seekers.add(new Seeker() {
            @Override
            public boolean seek() throws IOException {
                return YaTxtXmlTokenizer.this.parsePreamble();
            }
        });
    }

    public Token nextToken() {
        return parseNextToken();
    }

    private Token parseNextToken() {
        try {
            boolean tokenParsed = false;
            for (; !tokenParsed; ) {
                this.offset = charStream.getByteOffset();
                this.offsetCh = charStream.getCharOffset();
                ch = charStream.read();
                if (ch == -1) break;
                for (Seeker seeker : seekers) {
                    tokenParsed = seeker.seek();
                    if (tokenParsed) break;
                }
            }
            if (!tokenParsed) {
                token = EOF;
            }
            return token;
        } catch (IOException exc) {
            throw new XmlParsingException("unable to extract next token cause io exception: " + exc.getMessage());
        }
    }

    /**
     * Examines whether specified prefix pattern match further characters.
     *
     * @param p       Precompiled regex pattern.
     * @param prefLen The number of characters to look ahead.
     */
    private boolean checkPrefix(Pattern p, final int prefLen) throws IOException {
        final int utf8CharMaxSize = 4;
        int chtmp = ch;
        charStream.mark(prefLen * utf8CharMaxSize);
        char[] prefix = new char[prefLen];
        for (int i = 0; ch != -1 && i < prefLen; ++i) {
            prefix[i] = ((char) ch);
            ch = charStream.read();
        }
        ch = swap(chtmp, chtmp = ch);
        charStream.reset();
        return chtmp != -1 && p.matcher(CharBuffer.wrap(prefix)).matches();
    }

    private int swap(int b, int a) {
        return b;
    }

    private boolean parseOpenTag() throws IOException {
        final int prefLen = 2;
        boolean matchPrefix = checkPrefix(openTagPrefixPattern, prefLen);
        if (matchPrefix) {
            StringBuilder text = new StringBuilder();
            for (; ch != -1; ) {
                text.append((char) ch);
                if (ch == '>') break;
                ch = charStream.read();
            }
            int tokenLen = charStream.getByteOffset() - this.offset;
            int tokenChLen = charStream.getCharOffset() - this.offsetCh;
            token = new OpenTag(this.offset, tokenLen, this.offsetCh, tokenChLen, text.toString());
        }
        return matchPrefix;
    }

    private boolean parseCloseTag() throws IOException {
        final int prefLen = 2;
        boolean matchPrefix = checkPrefix(closeTagPrefixPattern, prefLen);
        if (matchPrefix) {
            StringBuilder text = new StringBuilder();
            for (; ch != -1; ) {
                text.append((char) ch);
                if (ch == '>') break;
                ch = charStream.read();
            }
            int len = charStream.getByteOffset() - this.offset;
            int tokenChLen = charStream.getCharOffset() - this.offsetCh;
            token = new CloseTag(this.offset, len, this.offsetCh, tokenChLen, text.toString());
        }
        return matchPrefix;
    }

    private boolean parseComment() {
        return false;
    }

    private boolean parseCDATA() {
        return false;
    }

    private boolean parsePreamble() {
        return false;
    }

    private interface Seeker {
        boolean seek() throws IOException;
    }

    public static abstract class Token {
        public final String text;
        public final FileLocation location;

        public Token(int off, int len, int chOff, int chLen, String text) {
            this.location = new FileLocation(off, len, chOff, chLen);
            this.text = text;
        }

        public FileLocation getLocation() {
            return location;
        }

        public String getText() {
            return text;
        }

        public boolean isOpenTag() {
            return false;
        }

        public boolean isCloseTag() {
            return false;
        }

        public boolean isPreamble() {
            return false;
        }

        public boolean isCDATA() {
            return false;
        }

        public boolean isComment() {
            return false;
        }

        public OpenTag asOpenTag() {
            return (OpenTag) this;
        }

        public CloseTag asCloseTag() {
            return (CloseTag) this;
        }

        @Override
        public int hashCode() {
            return location.hashCode();
        }

        @Override
        public boolean equals(Object other) {
            Token t = (Token) other;
            return (this == other) ||
                    (this.getClass().equals(other)) ||
                    (this.location.equals(t.location));
        }
    }

    /**
     * Placeholder class to indicate end of token stream.
     */
    private static class EOF extends Token {
        private EOF() {
            super(0, 0, 0, 0, null);
        }
    }

    public static class OpenTag extends Token {
        private String name = null;

        private static final String attrEntryRegex = "%s\\s*=\\s*\"[^\"]*\"";

        private static final Pattern valuePattern = Pattern.compile("\"[^\"]*\"");

        private OpenTag(int offset, int len, int chOff, int chLen, String text) {
            super(offset, len, chOff, chLen, text);
        }

        @Override
        public boolean isOpenTag() {
            return true;
        }

        public String getName() {
            if (name == null) {
                StringBuilder nameBuilder = new StringBuilder();
                for (int i = 1; i < text.length() && text.charAt(i) != ' '
                        && text.charAt(i) != '/' && text.charAt(i) != '>'; ++i) {
                    nameBuilder.append(text.charAt(i));
                }
                this.name = nameBuilder.toString();
            }
            return name;
        }

        public String getAttributeValue(String attrName) {
            Pattern p = Pattern.compile(String.format(attrEntryRegex, attrName));
            Matcher m = p.matcher(text);
            if (!m.find()) {
                throw new NoSuchAttributeException(String.format(
                        "attribute \"%s\" not found in \"%s\" token",
                        attrName, getName()));
            }
            String entry = text.substring(m.start(), m.end());
            m = valuePattern.matcher(entry);
            m.find(); // always true
            return entry.substring(m.start() + 1, m.end() - 1);
        }
    }

    public static class CloseTag extends Token {
        private String name = null;

        private CloseTag(int offset, int len, int chOff, int chLen, String text) {
            super(offset, len, chOff, chLen, text);
        }

        @Override
        public boolean isCloseTag() {
            return true;
        }

        public String getName() {
            if (name == null) {
                StringBuilder nameBuilder = new StringBuilder();
                for (int i = 2; i < text.length() && text.charAt(i) != ' ' && text.charAt(i) != '>'; ++i) {
                    nameBuilder.append(text.charAt(i));
                }
                this.name = nameBuilder.toString();
            }
            return name;
        }
    }
}
