package com.gitlab.yatxt.trigger.openweather;

public class Sys {
    public double type;
    public double id;
    public String country;
    public double sunrise;
    public double sunset;
}
