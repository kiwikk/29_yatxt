package com.gitlab.yatxt.book;

import com.gitlab.yatxt.exc.XmlParsingException;
import com.gitlab.yatxt.trigger.Trigger;
import com.gitlab.yatxt.trigger.TriggerFactory;
import com.gitlab.yatxt.utils.FileLocation;
import com.gitlab.yatxt.utils.IOUtils;
import com.gitlab.yatxt.utils.YaTxtXmlTokenizer;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

import static com.gitlab.yatxt.utils.YaTxtXmlTokenizer.OpenTag;
import static com.gitlab.yatxt.utils.YaTxtXmlTokenizer.Token;

/**
 * This class aimed to maintain section meta info.
 */
public class SectionTracker {
    public static final String JSON_SECTION_ARRAY_FIELD = "sections";

    private final LinkedList<Section> sections;
    private final TriggerFactory triggerFactory;
    private final Book book;

    public SectionTracker(Book book, TriggerFactory triggerFactory) {
        this.sections = new LinkedList<>();
        this.triggerFactory = triggerFactory;
        this.book = book;
    }

    public Section addSection(String sectionId) throws Exception {
        Section sec = readSectionMetaInfo(sectionId);
        sections.add(sec);
        return sec;
    }

    public List<String> getSectionIds(){
        LinkedList<String> sectionIds = new LinkedList<>();
        for(Section sec : sections){
            sectionIds.add(sec.id);
        }
        return sectionIds;
    }

    /**
     * Files only under 64kb are supported.
     */
    public static SectionTracker fromFile(Book book, TriggerFactory factory, File file) throws Exception {
        final int maxSize = 0xffff;
        if (file.length() > maxSize) {
            throw new IOException("attempt to open large file, files only under 64kb are supported");
        }
        String data = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        SectionTracker sectionTracker = new SectionTracker(book, factory);
        JSONObject branch = new JSONObject(data);
        JSONArray sections = branch.getJSONArray(JSON_SECTION_ARRAY_FIELD);
        for (int i = 0; i < sections.length(); ++i) {
            sectionTracker.sections.add(
                    sectionTracker.readSectionMetaInfo(sections.getString(i)));
        }
        return sectionTracker;
    }

    public String toJsonArray(){
        JsonArray arr = new JsonArray();
        for(Section sec : sections) {
            arr.add(sec.id);
        }
        JsonObject obj = new JsonObject();
        obj.add("sections", arr);
        return obj.toString();
    }

    private Section readSectionMetaInfo(String sectionId) throws Exception {
        return readSectionMetaInfo(new File(book.resolveSectionPath(sectionId)));
    }

    private Section readSectionMetaInfo(File secFile) throws Exception {
        if (!IOUtils.fileExists(secFile)) {
            throw new FileNotFoundException("the specified file name doesn't match any exists one.");
        }

        InputStream fin = null;
        Exception mainExc = null;
        try {
            Trigger tr = null;
            fin = new FileInputStream(secFile);
            YaTxtXmlTokenizer tokenizer = new YaTxtXmlTokenizer(fin);
            int textStart = 0, textLen = 0, textStartCh = 0, textLenCh = 0;
            Token token;
            for (; (token = tokenizer.nextToken()) != tokenizer.EOF; ) {
                if (token.isOpenTag()) {
                    OpenTag tag = token.asOpenTag();
                    String tagName = tag.getName();
                    switch (tagName) {
                        case Section.TRIGGER:
                            String trId = tag.getAttributeValue(Section.TRIGGER_ID);
                            tr = triggerFactory.newInstance(trId);
                            break;
                        case Section.SWITCH:
                            if (tr == null) {
                                throw new XmlParsingException(
                                        String.format("switch expression %s was declared out of trigger scope", tag.getText()));
                            }
                            String swCondition = tag.getAttributeValue(Section.SWITCH_CONDITION);
                            String swSectionId = tag.getAttributeValue(Section.SWITCH_SECTION_ID);
                            tr.addSwitch(swCondition, swSectionId);
                            break;
                        case Section.TEXT:
                            textStart = token.location.off + token.location.len;
                            textStartCh = token.location.offCh + token.location.lenCh;
                    }
                } else if (token.isCloseTag() && token.asCloseTag().getName().equals(Section.TEXT)) {
                    textLen = token.location.off - textStart;
                    textLenCh = token.location.offCh - textStartCh;
                    break;
                }
            }
            return new Section(FilenameUtils.removeExtension(secFile.getName()), tr,
                    new FileLocation(textStart, textLen, textStartCh, textLenCh));
        } catch (Exception e) {
            mainExc = e;
            throw mainExc;
        } finally {
            safeClose(fin, mainExc);
        }
    }

    private void safeClose(Closeable closeable, Exception mainExc) throws IOException {
        if (mainExc == null) {
            if (closeable != null) {
                closeable.close();
            }
        } else {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    public Section getSection(int pos) {
        return sections.get(pos);
    }

    public int getSize() {
        return sections.size();
    }

    public Section getLastSection() {
        return sections.getLast();
    }

    /**
     * This class define object wrapper for section meta-info.
     */
    public static class Section {
        public static final String TRIGGER = "trigger";
        public static final String TRIGGER_ID = "id";
        public static final String SWITCH = "switch";
        public static final String SWITCH_CONDITION = "condition";
        public static final String SWITCH_SECTION_ID = "sectionId";
        public static final String TEXT = "text";

        private final String id;
        private final Trigger trigger;
        private final FileLocation textLoc;

        private Section(String id, Trigger trigger, FileLocation textLoc) {
            this.id = id;
            this.trigger = trigger;
            this.textLoc = textLoc;
        }

        public String getId() {
            return id;
        }

        public Trigger getTrigger() {
            return trigger;
        }

        public FileLocation getTextLocation() {
            return textLoc;
        }

        @Override
        public String toString() {
            return String.format("section id : %s", id);
        }
    }
}
