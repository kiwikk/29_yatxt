package com.gitlab.yatxt.trigger;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeTrigger extends Trigger {
    private final String timeKey = "time";
    private Handler handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            Bundle data = msg.getData();
            String result = data.getString(timeKey);
            sendResult(result);
            return false;
        }
    });

    TimeTrigger(Context context) {
        super(context);
    }

    @Override
    public void addSwitch(String condition, String sectionId) {
        switches.add(new TimeSwitch(condition, sectionId));
    }

    @Override
    public void evaluate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

        Bundle data = new Bundle();
        data.putString(timeKey, dateFormat.format(new Date()));
        Message msg = new Message();
        msg.setData(data);
        handler.sendMessage(msg);
    }

    private class TimeSwitch extends Trigger.Switch {
        TimeSwitch(String condition, String sectionId) {
            super(condition, sectionId);
        }

        @Override
        public boolean matchCondition(String value) {
            if (condition.equals(DEFAULT_CONDITION)) {
                return true;
            }
            if (value.equals("err")) {
                return false;
            }

            String[] conditionTime = condition.split(":");
            String[] valueTime = value.split(":");

            try {
                if (Integer.parseInt(valueTime[0]) == Integer.parseInt(conditionTime[0])) {
                    return Integer.parseInt(valueTime[1]) <= Integer.parseInt(conditionTime[1]);
                }
                return Integer.parseInt(valueTime[0]) <= Integer.parseInt(conditionTime[0]);
            } catch (Throwable ignored) {
                return false;
            }
        }
    }
}
