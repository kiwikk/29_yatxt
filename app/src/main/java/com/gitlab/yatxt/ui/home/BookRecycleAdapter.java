package com.gitlab.yatxt.ui.home;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.yatxt.R;
import com.gitlab.yatxt.book.Book;

import java.io.File;
import java.util.List;

public class BookRecycleAdapter extends RecyclerView.Adapter<BookRecycleAdapter.BookHolder> {

    List<Book> books;

    BookRecycleAdapter(List<Book> books) {
        this.books = books;
    }

    @NonNull
    @Override
    public BookHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        BookHolder bh = new BookHolder(v);
        return bh;
    }

    @Override
    public void onBindViewHolder(@NonNull BookHolder holder, int position) {
        holder.mAuth.setText(books.get(position).getAuthor());
        holder.mtitle.setText(books.get(position).getBookName());

        File image = new File(books.get(position).getCover());
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);

        holder.imageView.setImageBitmap(bitmap);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public class BookHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView mtitle, mAuth;

        public BookHolder(@NonNull View itemView) {
            super(itemView);

            this.imageView = itemView.findViewById(R.id.imageView);
            this.mtitle = itemView.findViewById(R.id.title);
            this.mAuth = itemView.findViewById(R.id.author);

        }

    }

}
