package com.gitlab.yatxt.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.gitlab.yatxt.R;
import com.gitlab.yatxt.utils.IOUtils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import static com.gitlab.yatxt.YaTxtApplication.TAG;

public class TestActivity extends AppCompatActivity {
    private static final String APP_DIRECTORY = "ЯTXT";
    private static final String ASSETS_BOOKS = "books";

    private Runnable onPermissionResult;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_test_activity);
        Button loadButton = findViewById(R.id.button);
        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (requestPermissions()) {
                    onPermissionResult = new Runnable() {
                        @Override
                        public void run() {
                            read();
                        }
                    };
                } else {
                    read();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        onPermissionResult.run();
    }

    private void read() {
        try {
            String path = loadFromAssets();
            startReadActivity(path);
        } catch (IOException exc) {
            Log.e(TAG, "failed to load book from assets cause: " + exc.getMessage());
            exc.printStackTrace();
        }
    }

    private boolean requestPermissions() {
        List<String> permissions = new LinkedList<>();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!permissions.isEmpty()) {
            String[] p = new String[permissions.size()];
            permissions.toArray(p);
            requestPermissions(p, 0);
        }

        return !permissions.isEmpty();
    }

    private String loadFromAssets() throws IOException {
        AssetManager am = getAssets();
        String[] assetsBooks = am.list(ASSETS_BOOKS);
        if (assetsBooks.length == 0) {
            throw new IOException("assets/books directory was empty");
        }

        String bookName = assetsBooks[0];
        File appDir = new File(Environment.getExternalStorageDirectory(), APP_DIRECTORY);
        IOUtils.forceMkdirIfNotExists(appDir);
        File book = new File(appDir, bookName);
        if (IOUtils.dirExists(book)) {
            IOUtils.deleteDir(book);
        }
        copyDirFromAssets(ASSETS_BOOKS + "/" + bookName, book);
        return book.getAbsolutePath();
    }

    private void copyDirFromAssets(String src, File dest) throws IOException {
        AssetManager am = getAssets();
        String[] childs = am.list(src);
        if (childs == null) {
            throw new IOException("wtf");
        }
        if (childs.length > 0) {
            for (String child : childs) {
                File newDest = new File(dest, child);
                if (isDir(src + "/" + child)) {
                    FileUtils.forceMkdir(newDest);
                }
                copyDirFromAssets(src + "/" + child, newDest);
            }
        } else {
            InputStream in = am.open(src);
            FileUtils.copyInputStreamToFile(in, dest);
        }
    }

    private boolean isDir(String path) throws IOException {
        return getAssets().list(path).length > 0;
    }

    private void startReadActivity(String path) {
        Intent intent = new Intent(this, ReadActivity.class);
        intent.putExtra(ReadActivity.BOOK_PATH_MESSAGE, path);
        startActivity(intent);
    }
}
