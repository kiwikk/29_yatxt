package com.gitlab.yatxt.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.View;

public class TextLayoutView extends View {
    private Layout mLayout;

    public TextLayoutView(Context context) {
        this(context, null);
    }

    public TextLayoutView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextLayoutView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFocusable(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();
        if (mLayout != null) {
            canvas.translate(getPaddingLeft(), getPaddingTop());
            mLayout.draw(canvas);
        }

        canvas.restore();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (mLayout != null) {
            setMeasuredDimension(
                    getPaddingLeft() + getPaddingRight() + mLayout.getWidth(),
                    getPaddingTop() + getPaddingBottom() + mLayout.getHeight());
        }
    }

    public void setLayout(Layout layout) {
        mLayout = layout;
        requestLayout();
    }
}
