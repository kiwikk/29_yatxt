package com.gitlab.yatxt.pagesplitter;

import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.style.LeadingMarginSpan;

import com.gitlab.yatxt.book.SectionTracker;
import com.gitlab.yatxt.utils.FileLocation;
import com.gitlab.yatxt.utils.IOUtils;
import com.gitlab.yatxt.utils.RandomAccessFileInputAdapter;
import com.gitlab.yatxt.utils.RandomAccessFileOutputAdapter;
import com.gitlab.yatxt.utils.YaTxtCharInputStream;
import com.gitlab.yatxt.utils.YaTxtXmlTokenizer;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static com.gitlab.yatxt.utils.YaTxtXmlTokenizer.Token;

public class PuzzleChunkSupplier implements Closeable {
    private final RandomAccessFile raFile;
    private final PuzzleSplitStrategy splitStrategy;
    private final IndentSupplierImpl indentSupplier = new IndentSupplierImpl();
    private final LocationSupplierImpl locationSupplier = new LocationSupplierImpl();
    private final FilterImpl filter = new FilterImpl();
    private final DataConverter converter = new DataConverter();

    private PuzzleChunk prev = new PuzzleChunk(0, 0, 0, 0);

    private final Object lock = new Object();

    /**
     * @param chunkSize Chunk size in chars.
     */
    public PuzzleChunkSupplier(String filePath,
                               int chunkSize,
                               LayoutSupplier layoutSupplier) throws IOException {
        this(new File(filePath), chunkSize, layoutSupplier);
    }

    public PuzzleChunkSupplier(File file,
                               int chunkSize,
                               LayoutSupplier layoutSupplier) throws IOException {
        this.raFile = new RandomAccessFile(file, "rws");
        this.splitStrategy = new PuzzleSplitStrategy(chunkSize,
                filter,
                indentSupplier,
                locationSupplier,
                layoutSupplier);
    }

    public synchronized void attachFile(String filePath) throws IOException {
        File file = new File(filePath);
        if (!IOUtils.fileExists(file)) {
            throw new IOException("specified file path doesn't match any exists one");
        }
        if (file.length() > Integer.MAX_VALUE) {
            throw new IOException("attempt to open too large file, files only under 2GB are supported");
        }
        attachFile(filePath, 0, (int) file.length());
    }

    public synchronized void attachFile(String filePath, FileLocation loc) throws IOException {
        this.attachFile(filePath, loc.off, loc.getLen());
    }

    /**
     * @param offset Byte offset in source file from which required to start read.
     * @param len    Amount of bytes at least required to read.
     * @throws IOException
     */
    public synchronized void attachFile(String filePath, int offset, int len) throws IOException {
        File file = new File(filePath);
        if (!IOUtils.fileExists(file)) {
            throw new IOException("specified file path doesn't match any exists one");
        }
        if ((offset < 0) || (len < 0) || (offset + len > file.length())) {
            throw new IOException("specified file interval out of file content boundaries");
        }
        if (raFile.length() + (long) len > Integer.MAX_VALUE) {
            throw new IOException("file attaching will cause overflow, file only under 2GB are supported");
        }
        raFile.seek(raFile.length());
        BufferedWriter out = new BufferedWriter(
                new OutputStreamWriter(new RandomAccessFileOutputAdapter(raFile), StandardCharsets.UTF_8));
        converter.clear();

        YaTxtCharInputStream in = null;
        IOException mainExc = null;
        try {
            FileInputStream fin = new FileInputStream(file);
            fin.skip(offset);
            in = new YaTxtCharInputStream(fin);

            int ch;
            for (; in.getByteOffset() < len &&
                    (ch = in.read()) != -1; ) {
                int r = converter.convert((char) ch);
                if (r != -1) {
                    out.write(r);
                }
            }

            out.flush();
        } catch (IOException exc) {
            mainExc = exc;
            throw mainExc;
        } finally {
            safeClose(in, mainExc);
        }
    }

    private void safeClose(Closeable closeable, Exception mainExc) throws IOException {
        if (mainExc == null) {
            if (closeable != null) {
                closeable.close();
            }
        } else {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    private static class DataConverter {
        int state = 0;
        private static final int REPLACE = 0;
        private static final int COLLAPSE = 1;


        private final static Set<Character> SEPARATORS = new HashSet<Character>() {
            {
                add('\n');
                add('\t');
                add(' ');
            }
        };

        private final static Set<Character> IGNORABLE = new HashSet<Character>() {
            {
                add('\r');
            }
        };

        void clear() {
            state = REPLACE;
        }

        /**
         * @return replacement for specified character or -1 if character should be skipped
         */
        int convert(char ch) {
            if (IGNORABLE.contains(ch)) {
                return -1;
            }
            if (SEPARATORS.contains(ch)) {
                if (state > 0) {
                    return -1;
                }
                state = COLLAPSE;
                return ' ';
            } else {
                state = REPLACE;
                return ch;
            }
        }
    }

    public synchronized PuzzleChunk requestNextChunk() throws IOException {
        if (prev.getEnd() == raFile.length()) {
            return null;
        }

        final int start = prev.getEnd();
        raFile.seek(start);

        YaTxtCharInputStream in = new YaTxtCharInputStream(new RandomAccessFileInputAdapter(raFile));

        locationSupplier.setStream(in, start);
        indentSupplier.setPrevChunk(prev);

        splitStrategy.clear();
        splitStrategy.start();

        int ch;
        while (!splitStrategy.full() && (ch = in.read()) != -1) {
            splitStrategy.append(ch);
        }

        prev = splitStrategy.build();
        return prev;
    }


    public CharSequence loadChunk(PuzzleChunk chunk) throws IOException {
        byte[] data = new byte[chunk.getLen()];
        IOUtils.read(raFile, data, 0, chunk.getLen(), chunk.getStart());
        return new String(data, StandardCharsets.UTF_8);
    }

    @Override
    public void close() throws IOException {
        if (raFile != null) {
            raFile.close();
        }
    }

    public static class PuzzleChunk {
        private final int start;
        /**
         * Index of byte after last byte owned by chunk.
         */
        private final int end;
        private final float indentWidth;
        private final float bottomWidth;

        private PuzzleChunk(int start, int end, float indentWidth, float bottomWidth) {
            this.start = start;
            this.end = end;
            this.indentWidth = indentWidth;
            this.bottomWidth = bottomWidth;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }

        public int getLen() {
            return end - start;
        }

        public float getIndentWidth() {
            return indentWidth;
        }

        public float getBottomWidth() {
            return bottomWidth;
        }

        @Override
        public int hashCode() {
            int offhash = (start & 0x8fffffff) % Short.MAX_VALUE;
            int lenhash = (end & 0x8fffffff) % Short.MAX_VALUE;
            return offhash << 16 + lenhash;
        }

        @Override
        public boolean equals(Object obj) {
            PuzzleChunk c = (PuzzleChunk) obj;
            return (this == obj) || (this.start == c.start && this.end == c.end);
        }

        @Override
        public String toString() {
            return String.format(Locale.ENGLISH, "start : %d, end : %d, len : %d", start, end, getLen());
        }
    }

    private static class PuzzleSplitStrategy {
        private final Filter filter;
        private final IndentSupplier indentSupplier;
        private final LocationSupplier locationSupplier;
        private final LayoutSupplier layoutSupplier;
        private final LayoutCache layoutCache = LayoutCache.get();

        private SpannableStringBuilder buff; // maybe replace with StringBuffer ?

        private int charsCounter = 0;
        private final int size; // in char units
        private int startOffset;
        private int endOffset;
        private float indentWidth;

        private State state = State.CLEAR;

        private static final Set<Character> SEPARATORS = new HashSet<>();

        static {
            SEPARATORS.add(' ');
            SEPARATORS.add(',');
            SEPARATORS.add('.');
            SEPARATORS.add(';');
            SEPARATORS.add(':');
            SEPARATORS.add('-');
            SEPARATORS.add('!');
            SEPARATORS.add('?');
            SEPARATORS.add('+');
            SEPARATORS.add('*');
            SEPARATORS.add('%');
            SEPARATORS.add('/');
            SEPARATORS.add('\\');
            SEPARATORS.add('}');
            SEPARATORS.add('{');
            SEPARATORS.add(']');
            SEPARATORS.add('[');
            SEPARATORS.add(')');
            SEPARATORS.add('(');
        }

        public interface Filter {
            boolean ignore(int ch);
        }

        public interface IndentSupplier {
            float supply();
        }

        public interface LocationSupplier {
            int supply();
        }

        private enum State {
            CLEAR,
            PROCESSING,
            SEEK_FOR_SEPARATOR,
            FULL
        }

        public PuzzleSplitStrategy(int size, Filter filter, IndentSupplier indentSupplier,
                                   LocationSupplier locationSupplier, LayoutSupplier layoutSupplier) {

            this.buff = new SpannableStringBuilder();
            this.size = size;
            this.filter = filter;
            this.indentSupplier = indentSupplier;
            this.locationSupplier = locationSupplier;
            this.layoutSupplier = layoutSupplier;
        }

        public void start() {
            if (!state.equals(State.CLEAR)) {
                throw new IllegalStateException("illegal calling of start method before state was clear");
            }
            startOffset = locationSupplier.supply();
            state = State.PROCESSING;
        }

        public void append(int ch) {
            if (!filter.ignore(ch)) {
                rout(ch);
            }
        }

        private void rout(int ch) {
            switch (state) {
                case PROCESSING:
                    appendProcessing(ch);
                    break;
                case SEEK_FOR_SEPARATOR:
                    appendSeekForSeparator(ch);
                    break;
                default:
                    throw new IllegalStateException("attempt to append character with inappropriate state");
            }
        }

        private void appendProcessing(int ch) {
            if (charsCounter == size - 1) {
                state = State.SEEK_FOR_SEPARATOR;
                rout(ch);
            } else {
                buff.append((char) ch);
                charsCounter += 1;
            }
        }

        private void appendSeekForSeparator(int ch) {
            buff.append((char) ch);
            if (isSeparator(ch)) {
                state = State.FULL;
            }
        }

        private boolean isSeparator(int ch) {
            return SEPARATORS.contains((char) ch);
        }

        public boolean full() {
            return state.equals(State.FULL);
        }

        public PuzzleChunk build() {
            this.endOffset = locationSupplier.supply();
            this.indentWidth = resolveIndentWidth(indentSupplier.supply());

            SpannableStringBuilder spanText = new SpannableStringBuilder();
            if (indentWidth == 0) {
                spanText.append('\n');
            }
            spanText.append(buff);
            spanText.setSpan(new LeadingMarginSpan.Standard((int) indentWidth, 0), 0, spanText.length(), 0);
            StaticLayout layout = layoutSupplier.obtain(spanText);

            PuzzleChunk chunk = new PuzzleChunk(startOffset, endOffset, indentWidth, measureBottomWidth(layout));
            layoutCache.put(chunk, layout);
            return chunk;
        }

        private float resolveIndentWidth(float indentWidth) {
            if (indentWidth == 0) {
                return indentWidth;
            }
            int i = 0;
            for (; i < buff.length(); ++i) {
                if (isSeparator(buff.charAt(i))) break;
            }
            CharSequence head = buff.subSequence(0, i);
            TextPaint paint = layoutSupplier.getPaint();
            float headWidth = paint.measureText(head.toString());
            float emptySpace = layoutSupplier.getWidth() - indentWidth;
            return ((headWidth > emptySpace) ? 0 : indentWidth);
        }

        private float measureBottomWidth(StaticLayout layout) {
            return layout.getLineWidth(layout.getLineCount() - 1);
        }

        public void clear() {
            charsCounter = 0;
            buff.clear();
            buff.clearSpans();
            state = State.CLEAR;
        }
    }

    private static class IndentSupplierImpl implements PuzzleSplitStrategy.IndentSupplier {
        PuzzleChunk prev;

        void setPrevChunk(PuzzleChunk prev) {
            this.prev = prev;
        }

        @Override
        public float supply() {
            return prev.bottomWidth;
        }
    }

    private static class LocationSupplierImpl implements PuzzleSplitStrategy.LocationSupplier {
        YaTxtCharInputStream stream;
        int offset = 0;

        void setStream(YaTxtCharInputStream stream, int offset) {
            this.stream = stream;
            this.offset = offset;
        }

        public int supply() {
            return offset + stream.getByteOffset();
        }
    }

    private static class FilterImpl implements PuzzleSplitStrategy.Filter {
        @Override
        public boolean ignore(int ch) {
            return false;
        }

    }

    private static List<Token> getTextScopeTokens(String filepath) throws Exception {
        InputStream fin = null;
        Exception mainExc = null;
        try {
            fin = new FileInputStream(filepath);
            YaTxtXmlTokenizer tokenizer = new YaTxtXmlTokenizer(fin);
            List<Token> tokens = new ArrayList<>();

            boolean textScope = false;
            Token token;
            for (; !(token = tokenizer.nextToken()).equals(tokenizer.EOF) && !(token.isCloseTag() && token.asCloseTag().getName().equals(SectionTracker.Section.TEXT)); ) {

                if (token.isOpenTag() && token.asOpenTag().getName().equals(SectionTracker.Section.TEXT)) {
                    textScope = true;
                }

                if (textScope && (token.isOpenTag() || token.isCloseTag())) {
                    tokens.add(token);
                }
            }
            tokens.add(token);
            return tokens;
        } catch (Exception e) {
            mainExc = e;
            throw mainExc;
        } finally {
            if (mainExc == null) {
                fin.close();
            } else {
                if (fin != null) {
                    try {
                        fin.close();
                    } catch (IOException ignore) {
                    }
                }
            }
        }
    }
}
