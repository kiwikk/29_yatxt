package com.gitlab.yatxt.exc;

public class InvalidBookInfoException extends Exception {
    public InvalidBookInfoException() {
        super();
    }

    public InvalidBookInfoException(String message) {
        super(message);
    }
}
