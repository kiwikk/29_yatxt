package com.gitlab.yatxt.trigger;

import android.content.ContentProvider;
import android.content.Context;

import java.util.LinkedList;

public abstract class Trigger {
    protected final LinkedList<Switch> switches = new LinkedList<>();
    protected final LinkedList<OnTriggerEvaluatedListener> listeners = new LinkedList<>();
    protected final Context context;

    Trigger(Context context) {
        this.context = context;
    }

    public interface OnTriggerEvaluatedListener {
        void onEvaluated(String result);
    }

    public abstract void addSwitch(String condition, String sectionId);

    public abstract void evaluate();

    public final void addOnTriggerEvaluatedListener(OnTriggerEvaluatedListener listener) {
        listeners.add(listener);
    }

    public final Switch firstMatchingSwitch(String value) {
        for (Switch sw : switches) {
            if (sw.matchCondition(value)) {
                return sw;
            }
        }
        throw new RuntimeException("No switches matching specified value");
    }

    protected final void sendResult(String result) {
        for (OnTriggerEvaluatedListener listener : listeners) {
            listener.onEvaluated(result);
        }
    }

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format("<trigger id=%s>%n", getClass().toString()));
        for (Switch sw : switches) {
            strBuilder.append(sw.toString());
            strBuilder.append(System.lineSeparator());
        }
        strBuilder.append("</trigger>");
        return strBuilder.toString();
    }

    public abstract class Switch {
        protected final String condition;
        protected final String sectionId;

        protected static final String DEFAULT_CONDITION = "default";

        protected Switch(String condition, String sectionId) {
            this.condition = condition.toLowerCase();
            this.sectionId = sectionId.toLowerCase();
        }

        /**
         * @return Whether switch condition satisfies specified value.
         */
        public boolean matchCondition(String value) {
            return condition.equals(DEFAULT_CONDITION) || condition.equals(value.toLowerCase());
        }

        public final String getCondition() {
            return condition;
        }

        public final String getSectionId() {
            return sectionId;
        }

        @Override
        public String toString() {
            return String.format("condition=%s, sectionId=%s", condition, sectionId);
        }
    }
}