package com.gitlab.yatxt.trigger;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

public class SampleTrigger extends Trigger {

    SampleTrigger(Context context) {
        super(context);
    }

    @Override
    public void addSwitch(String condition, String sectionId) {
        switches.add(new SampleSwitch(condition, sectionId));
    }

    @Override
    public void evaluate() {
        // какие-то вычисления

        // в коллбэке который срабатывает по завершению вычислений нужно вызвать
        // что-то типа sendResult(convertToString(result))
    }

    private String convertToString(double value) {
        return "value converted to string";
    }

    private class SampleSwitch extends Trigger.Switch {

        SampleSwitch(String condition, String sectionId) {
            super(condition, sectionId);
        }

        @Override
        public boolean matchCondition(String value) {

            return super.matchCondition(value);
        }
    }
}
