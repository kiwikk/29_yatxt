package com.gitlab.yatxt.ui.library;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.yatxt.R;
import com.gitlab.yatxt.ui.library.description.ItemClickListener;

public class LibraryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    ImageView imageView;
    TextView mtitle, mAuth;
    ItemClickListener itemClickListener;

    public LibraryHolder(@NonNull View itemView) {
        super(itemView);

        this.imageView = itemView.findViewById(R.id.imageView);
        this.mtitle = itemView.findViewById(R.id.title);
        this.mAuth = itemView.findViewById(R.id.author);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.OnItemClickListener(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
