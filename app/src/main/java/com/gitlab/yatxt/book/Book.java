package com.gitlab.yatxt.book;

import android.util.Log;

import androidx.annotation.NonNull;

import com.gitlab.yatxt.exc.InvalidBookInfoException;
import com.gitlab.yatxt.exc.RootDirectoryNotFoundException;
import com.gitlab.yatxt.exc.BookInfoNotFoundException;
import com.gitlab.yatxt.utils.IOUtils;

import org.jetbrains.annotations.NotNull;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Book {
    private String id;
    private String rootDir;
    private String bootSectionId;
    private String description;
    private String bookName;
    private String cover;
    private String author;
    private String genre;
    private String[] is;
    private boolean isFinished = false;

    private static final String INFO_FILE_NAME = "book-info.xml";
    private static final String BOOK_ID_TAG = "bookId";
    private static final String BOOT_SECTION_ID_TAG = "bootSectionId";
    private static final String DESCRIPTION_TAG = "description";
    private static final String BOOK_NAME_TAG = "bookName";
    private static final String COVER_TAG = "cover";
    private static final String AUTHOR_TAG = "author";
    private static final String GENRE_TAG = "genre";

    private static final String BOOK_ID_ATTR_VALUE = "value";
    private static final String BOOT_SECTION_ID_ATTR_VALUE = "value";
    private static final String BOOK_NAME_ATTR_VALUE = "value";
    private static final String COVER_ATTR_VALUE = "value";
    private static final String AUTHOR_ATTR_VALUE = "value";
    private static final String GENRE_ATTR_VALUE = "value";

    public Book() {
    }

    private Book(String rootDir) {
        this.rootDir = rootDir;
    }

    public static Book fromRootDir(String rootDir) throws Exception {
        File root = new File(rootDir);
        if (!IOUtils.dirExists(root)) {
            throw new RootDirectoryNotFoundException("specified root directory doesn't match any exists one");
        }
        File info = IOUtils.getChild(root, INFO_FILE_NAME);
        if (!IOUtils.fileExists(info)) {
            throw new BookInfoNotFoundException("book info file doesn't exist");
        }
        Book book = new Book(rootDir);
        InputStream in = null;
        Exception mainExc = null;
        try {
            in = new FileInputStream(info);
            XmlPullParser xmlParser = XmlPullParserFactory.newInstance().newPullParser();
            xmlParser.setInput(in, "utf-8");
            int eventType = xmlParser.next();

            for (; eventType != XmlPullParser.END_DOCUMENT; ) {
                if (xmlParser.getEventType() == XmlPullParser.START_TAG) {
                    switch (xmlParser.getName()) {
                        case BOOK_ID_TAG:
                            book.id = xmlParser.getAttributeValue(null, BOOK_ID_ATTR_VALUE);
                            break;
                        case BOOT_SECTION_ID_TAG:
                            book.bootSectionId = xmlParser.getAttributeValue(null, BOOT_SECTION_ID_ATTR_VALUE);
                            break;
                        case DESCRIPTION_TAG:
                            eventType = xmlParser.next();
                            if (eventType == XmlPullParser.TEXT) {
                                String description = xmlParser.getText();
                                description = replaceLeadingLineBreaks(description);
                                description = replaceLeadingLineBreaks(new StringBuilder(description).reverse().toString());
                                book.description = new StringBuilder(description).reverse().toString();
                                break;
                            } else {
                                continue; // prevents missing tag after description tag
                            }
                        case BOOK_NAME_TAG:
                            book.bookName = xmlParser.getAttributeValue(null, BOOK_NAME_ATTR_VALUE);
                            break;
                        case COVER_TAG:
                            book.cover = xmlParser.getAttributeValue(null, COVER_ATTR_VALUE);
                            break;
                    }
                }
                eventType = xmlParser.next();
            }
            validate(book);
            return book;
        } catch (XmlPullParserException | IOException exc) {
            mainExc = exc;
            throw mainExc;
        } finally {
            safeClose(in, mainExc);
        }
    }

    private static String replaceLeadingLineBreaks(String str) {
        int leadingLineBreaks = 0;
        for (; leadingLineBreaks < str.length(); ) {
            char ch = str.charAt(leadingLineBreaks);
            if (ch != '\n' && ch != '\t' && ch != ' ') break;
            ++leadingLineBreaks;
        }
        return str.substring(leadingLineBreaks);
    }

    private static void validate(Book book) throws InvalidBookInfoException {
        if (book.rootDir == null) {
            throw new InvalidBookInfoException("missing book root directory");
        }
        if (book.id == null) {
            throw new InvalidBookInfoException("missing book id");
        }
        if (book.bootSectionId == null) {
            throw new InvalidBookInfoException("missing book boot section id");
        }
        if (book.bookName == null) {
            throw new InvalidBookInfoException("missing book name");
        }
        if (book.cover == null) {
            throw new InvalidBookInfoException("missing cover");
        }
    }

    private static void safeClose(Closeable closeable, Exception mainExc) throws IOException {
        if (mainExc == null) {
            if (closeable != null) {
                closeable.close();
            }
        } else {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    public void getInputStream(String book_name) {
        String url = "https://kiwikk.pythonanywhere.com/name?name=" + book_name;

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();

        is = new String[1];
        try {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    Log.d("OkHttpClient", "Book 61");
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code" + response);
                    else {
                        is[0] = response.body().string();
                        isFinished = true;
                    }
                }
            });
        } catch (Exception e) {
            Log.e("Exception Book 49", e.getMessage());
            e.printStackTrace();
        }

    }

    public void parseDescription(String is) {
        try {
            XmlPullParser xmlPullParser = XmlPullParserFactory.newInstance().newPullParser();
            xmlPullParser.setInput(new StringReader(is));

            for (int evType = xmlPullParser.next(); evType != XmlPullParser.END_DOCUMENT; ) {
                if (xmlPullParser.getEventType() == XmlPullParser.START_TAG) {
                    switch (xmlPullParser.getName()) {
                        case BOOK_ID_TAG:
                            this.id= xmlPullParser.getAttributeValue(null, BOOK_ID_ATTR_VALUE);
                            break;
                        case BOOK_NAME_TAG:
                            this.bookName = xmlPullParser.getAttributeValue(null, BOOK_NAME_ATTR_VALUE);
                            break;
                        case AUTHOR_TAG:
                            this.author = xmlPullParser.getAttributeValue(null, AUTHOR_ATTR_VALUE);
                            break;
                        case GENRE_TAG:
                            this.genre = xmlPullParser.getAttributeValue(null, GENRE_ATTR_VALUE);
                            break;
                        case DESCRIPTION_TAG:
                            evType = xmlPullParser.next();
                            if (evType == XmlPullParser.TEXT) {
                                String description = xmlPullParser.getText();
                                description = replaceLeadingLineBreaks(description);
                                description = replaceLeadingLineBreaks(new StringBuilder(description).reverse().toString());
                                this.description = new StringBuilder(description).reverse().toString();
                                break;
                            }
                    }
                }
                evType = xmlPullParser.next();
            }
        } catch (XmlPullParserException | IOException e) {
            Log.e("Book 78 XMLPullParser", e.getMessage());
            e.printStackTrace();
        }
    }

    public String getRootDir() {
        return rootDir;
    }

    public String resolveSectionPath(String sectionId) {
        return (rootDir + File.separator + sectionId + ".xml");
    }

    public String getId() {
        return id;
    }

    public String getBootSectionId() {
        return bootSectionId;
    }

    public String getDescription() {
        return description;
    }

    public String getBookName() {
        return bookName;
    }

    public String getCover() {
        return cover;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescriptionFromXml() {
        return is[0];
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public boolean isFinished() {
        return isFinished;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("URI: %s%nID: %s%nBOOT SECTION: %s%nBOOK NAME: %s%nCOVER: %s%n",
                rootDir, id, bootSectionId, bookName, cover);
    }
}
