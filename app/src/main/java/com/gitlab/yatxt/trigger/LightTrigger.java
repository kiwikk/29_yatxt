package com.gitlab.yatxt.trigger;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import java.util.Arrays;

public class LightTrigger extends Trigger {
    private SensorManager sensorManager;

    private final String lightKey = "light";
    Handler handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            Bundle data = msg.getData();
            String result = data.getString(lightKey);
            sendResult(result);
            return false;
        }
    });

    private void sendErrResult() {
        Bundle data = new Bundle();
        data.putString(lightKey, "err");
        Message msg = new Message();
        msg.setData(data);
        handler.sendMessage(msg);
    }

    LightTrigger(Context context) {
        super(context);
    }

    @Override
    public void addSwitch(String condition, String sectionId) {
        switches.add(new LightSwitch(condition, sectionId));
    }

    @Override
    public void evaluate() {
        try {
            sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
            Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
            if (lightSensor == null) {
                sendErrResult();
                return;
            }

            sensorManager.registerListener(this.new LightListener(), lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
        } catch (Throwable ignored) {
            sendErrResult();
        }
    }

    private class LightListener implements SensorEventListener {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            Sensor triggeredSensor = sensorEvent.sensor;

            if (triggeredSensor.getType() == Sensor.TYPE_LIGHT) {
                sensorManager.unregisterListener(this);
                sendResult(Double.toString(sensorEvent.values[0]));
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }

    private class LightSwitch extends Trigger.Switch {
        LightSwitch(String condition, String sectionId) {
            super(condition, sectionId);
        }

        @Override
        public boolean matchCondition(String value) {
            if (condition.equals(DEFAULT_CONDITION)) {
                return true;
            }
            if (value.equals("err")) {
                return false;
            }

            try {
                return Double.parseDouble(value) <= Double.parseDouble(condition);
            } catch (Throwable ignored) {
                return false;
            }
        }
    }
}
