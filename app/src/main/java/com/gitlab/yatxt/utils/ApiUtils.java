package com.gitlab.yatxt.utils;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.gitlab.yatxt.YaTxtApplication;
import com.gitlab.yatxt.book.Book;
import com.gitlab.yatxt.exc.BookAlreadyExistsException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class ApiUtils {
    public static final String ROOT_URL = "https://kiwikk.pythonanywhere.com/";
    public static final String LIST_BOOK_CONTENT_REQUEST = "pages?name=%s";
    public static final String GET_BOOK_PAGE_REQUEST = "download?name=%s&file=%s";

    private static final ApiUtils singleton = new ApiUtils();

    private ExecutorService worker = Executors.newFixedThreadPool(1);

    private ApiUtils() {

    }

    public static ApiUtils getInstance() {
        return singleton;
    }

    public void requestBookDownload(final Context context, final String bookName, final Runnable onComplete) {
        worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    downloadBook(context, bookName, onComplete);
                } catch (IOException e) {

                } catch (BookAlreadyExistsException e) {

                }
            }
        });
    }

    private static String doGet(URL endpoint, Map<String, String> headers) {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) endpoint.openConnection();
            connection.setRequestMethod("GET");
            connection.setUseCaches(false);

            for (Map.Entry<String, String> header : headers.entrySet()) {
                connection.addRequestProperty(header.getKey(), header.getValue());
            }

            InputStream in = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
            StringBuilder payload = new StringBuilder();

            String line;
            for (; (line = reader.readLine()) != null; ) {
                payload.append(line);
            }

            reader.close();

            return payload.toString();

        } catch (IOException ignore) {
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return "";
    }

    private void downloadBook(Context context, String bookName, final Runnable onComplete) throws IOException, BookAlreadyExistsException {
        File appDir = YaTxtApplication.getAppDir();
        File bookDir = new File(appDir, bookName);
        final String[] content = listBookContent(bookName);
        IOUtils.forceMkdirIfNotExists(bookDir);
        BroadcastReceiver progressChecker = new BroadcastReceiver() {
            Object lock = new Object();
            volatile int counter = content.length;

            @Override
            public void onReceive(Context context, Intent intent) {
                counter -= 1;
                if (counter <= 0) {
                    onComplete.run();
                }
            }
        };
        context.registerReceiver(progressChecker, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        for (String page : content) {
            DownloadManager.Request downloadRequest = new DownloadManager
                    .Request(Uri.parse(ROOT_URL + String.format(GET_BOOK_PAGE_REQUEST, bookName, page)))
                    .setDestinationUri(Uri.fromFile(new File(bookDir, page)))
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);

            DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            downloadManager.enqueue(downloadRequest);
        }
    }

    public String[] listBookContent(String bookName) {
        String url = ROOT_URL + String.format(LIST_BOOK_CONTENT_REQUEST, bookName);
        String response = "";
        try {
            response = doGet(new URL(url), new HashMap<String, String>());
        } catch (MalformedURLException ignore) {
            Log.d(YaTxtApplication.TAG, String.format("malformed url %s", url));
        }
        return response.trim().split(" ");
    }
}
