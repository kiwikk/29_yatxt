package com.gitlab.yatxt.utils;

public class FileLocation {
    public final int off;
    public final int len;
    public final int offCh;
    public final int lenCh; // ha-ha

    public FileLocation(int off, int len, int offCh, int lenCh) {
        this.off = off;
        this.len = len;
        this.offCh = offCh;
        this.lenCh = lenCh;
    }

    public int getLen() {
        return len;
    }

    public int getLenCh() {
        return lenCh;
    }

    public int getOff() {
        return off;
    }

    public int getOffCh() {
        return offCh;
    }

    public int getEnd() {
        return off + len;
    }

    public int getEndCh() {
        return offCh + lenCh;
    }

    @Override
    public int hashCode() {
        int offHash = (off & 0x8fffffff) % Short.MAX_VALUE;
        int lenHash = (len & 0x8fffffff) % Short.MAX_VALUE;
        return offHash << 16 + lenHash;
    }

    @Override
    public boolean equals(Object obj) {
        FileLocation l = (FileLocation) obj;
        return (this == obj) ||
                (this.off == l.off && this.len == l.len);
    }
}
