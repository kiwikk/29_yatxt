package com.gitlab.yatxt.ui.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.gitlab.yatxt.R;

public class SettingsFragment extends Fragment {

    private SettingsViewModel settingsViewModel;
    private Button profile_settings;
    private Button notifications;
    private Button about;
    private Button get_developers;
    private Button become_author;
    private Button exit;
    private TextView text;
    private boolean state;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        settingsViewModel =
                ViewModelProviders.of(this).get(SettingsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        profile_settings = root.findViewById(R.id.profile_settings);
        setListener(profile_settings, "Настройки профиля","Эта секция будет добавлена в следующих версиях", 1);

        notifications = root.findViewById(R.id.notifications);
        setListener(notifications, "Уведомления","Эта секция будет добавлена в следующих версиях", 1);

        about = root.findViewById(R.id.about);
        setListener(about, "О приложении","ЯТХТ\nВзгляните на старые книги под новым углом ", 1);


        get_developers = root.findViewById(R.id.get_developers);
        setListener(get_developers, "Разработчики", "Telegram:\nДенис Чертанов: @Denistyta\n" +
                "Николай Сафонов: @nockmok\n" +
                "Лиза Фролова: @kiwikk (TL)",1);
        become_author = root.findViewById(R.id.become_author);
        setListener(become_author, "Стать автором", "Свяжитесь с заказчиком проекта - (TG) @yan_ra",1);
        exit = root.findViewById(R.id.exit);
        setListener(exit, "Выход", "Вы уверены, что хотите выйти?", 0);

        return root;
    }


    void setListener(Button button, final String title, final String message, final int type) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(title);
                alert.setMessage(message);
                alert.setPositiveButton("OK", clickListener);
                if (type == 0) alert.setNegativeButton("No", clickListener);
                alert.show();

            }

            DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case Dialog.BUTTON_POSITIVE:
                            state = true;
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            state = false;
                            break;
                    }
                    doAccordingTo(type);
                }
            };
        });
    }

    void doAccordingTo(int type) {
        if (type == 0 && state) {
            getActivity().finish();
            System.exit(0);
        }
    }
}