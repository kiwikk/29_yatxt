package com.gitlab.yatxt.exc;

import java.io.IOException;

public class RootDirectoryNotFoundException extends IOException {
    public RootDirectoryNotFoundException() {
        super();
    }

    public RootDirectoryNotFoundException(String message) {
        super(message);
    }
}
