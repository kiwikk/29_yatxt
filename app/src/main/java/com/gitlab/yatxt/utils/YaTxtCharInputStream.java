package com.gitlab.yatxt.utils;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * This class wraps utf8 char (supported only characters fit in first 2 byte) input stream
 * to provide byte offset for each char read.
 * This class maintains internal buffer, using to smooth reading from file and to support markable interface as well.
 */
public class YaTxtCharInputStream extends FilterInputStream {
    private int offset = 0;
    private int charOffset = 0;
    private final InputStream in;
    private byte[] buff;
    private int buffPos;
    private int bytesBuffered;
    private final byte[] utf8Bytes = new byte[4];

    private int markLen;
    private int markPos;
    private int markOff;
    private int markChOff;

    private static final int BUFF_SIZE = 8192;

    public YaTxtCharInputStream(InputStream in) {
        super(in);
        this.buff = new byte[BUFF_SIZE];
        this.buffPos = 0;
        this.bytesBuffered = 0;
        this.in = in;
    }

    /**
     * performs reading from buffer and initiates buffer reloading if desired.
     */
    private int nextByte() throws IOException {
        if (buffPos >= bytesBuffered) {
            bytesBuffered = in.read(buff, 0, BUFF_SIZE);
            if (bytesBuffered == -1) {
                return -1;
            }
            buffPos = 0;
        }
        int b = toUByte(buff[buffPos]);
        buffPos += 1;
        offset += 1;
        markLen -= (markLen < 0) ? 0 : 1;
        return b;
    }

    /**
     * Read next char from input stream.
     *
     * @return java char (or unicode code point) wrapped in int, either -1 if eof reached.
     */
    @Override
    public synchronized int read() throws IOException {
        char ch;
        int b = nextByte();
        int bLen = getUTF8charLen(b);
        Arrays.fill(utf8Bytes, (byte) 0);
        utf8Bytes[4 - bLen] = (byte) b;
        for (int i = 4 - bLen + 1; i <= 3; ++i) {
            int nb = nextByte();
            if (nb == -1) {
                return -1;
            }
            utf8Bytes[i] = (byte) nb;
        }
        int utf8Int = getUTF8Int(utf8Bytes);
        ch = decodeUTF8(utf8Int);
        charOffset += 1;
        return ch;
    }

    private char decodeUTF8(int b) {
        char ch = 0;
        if ((0xff00 & b) == 0) { // ascii char
            ch = (char) b;
        } else {
            int byte2mask = 0x1f00;
            int byte3mask = 0x3f;
            ch |= ((byte2mask & b) >>> 2);
            ch |= (byte3mask & b);
        }
        return ch;
    }

    /**
     * convert consumed 4 bytes to int representation
     */
    private int getUTF8Int(byte[] bytes) {
        int i = 0;
        i |= (toUByte(bytes[0]) << (3 * Byte.SIZE));
        i |= (toUByte(bytes[1]) << (2 * Byte.SIZE));
        i |= (toUByte(bytes[2]) << Byte.SIZE);
        i |= toUByte(bytes[3]);
        return i;
    }

    /**
     * returns value in range [0, 255] wrapped to int.
     */
    private int toUByte(byte b) {
        return (b < 0) ? b + 256 : b;
    }

    /**
     * consume first byte of utf8 char to guess its byte length.
     */
    private int getUTF8charLen(int b) {
        int len;
        int mask = 0xf0 & b;
        if (mask <= 0x70) {
            len = 1;
        } else if (mask <= 0xb0) {
            throw new IllegalArgumentException("illegal byte format");
        } else if (mask <= 0xd0) {
            len = 2;
        } else if (mask <= 0xe0) {
            len = 3;
        } else {
            len = 4;
        }
        return len;
    }

    /**
     * @param readlimit Amount of buffered <b>bytes</b> desired.
     */
    @Override
    public synchronized void mark(int readlimit) {
        if (readlimit > BUFF_SIZE) {
            throw new IllegalArgumentException(String.format("supported only read limits <= %d", BUFF_SIZE));
        }
        try {
            markLen = readlimit;
            if (bytesBuffered == BUFF_SIZE && BUFF_SIZE - buffPos < readlimit) { // no eof reached
                alignBuffer();
                int buffOff = bytesBuffered;
                bytesBuffered += Math.max(0, in.read(buff, buffOff, BUFF_SIZE - buffOff));
                markLen = bytesBuffered;
            }
            markPos = buffPos;
            markOff = offset;
            markChOff = charOffset;
        } catch (IOException suppressed) {
        }
    }

    /**
     * Shift buffer values left to first necessary value.
     */
    private void alignBuffer() {
        int i = buffPos;
        int j = 0;
        for (; i < bytesBuffered; ++i, ++j) {
            buff[j] = buff[i];
        }
        bytesBuffered = j;
        buffPos = 0;
    }

    @Override
    public synchronized void reset() throws IOException {
        if (markLen < 0) {
            throw new IOException("stream not marked.");
        }
        buffPos = markPos;
        offset = markOff;
        charOffset = markChOff;
    }

    public int getByteOffset() {
        return offset;
    }

    public int getCharOffset() {
        return charOffset;
    }

    @Override
    public boolean markSupported() {
        return true;
    }

    @Override
    public void close() throws IOException {
        super.close();
    }

    @Override
    public long skip(long n) throws IOException {
        throw new UnsupportedOperationException("skip call may cause unpredictable behaviour");
    }
}
