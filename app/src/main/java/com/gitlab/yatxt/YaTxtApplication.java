package com.gitlab.yatxt;

import android.app.Application;
import android.os.Environment;

import java.io.File;

public class YaTxtApplication extends Application {
    public static final String TAG = "--->";
    private static final String APP_DIRECTORY_NAME = "ЯTXT";
    public static File getAppDir(){
        return new File(Environment.getExternalStorageDirectory(), APP_DIRECTORY_NAME);
    }
}
