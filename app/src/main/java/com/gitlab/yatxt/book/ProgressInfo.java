package com.gitlab.yatxt.book;

import com.gitlab.yatxt.utils.IOUtils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Scanner;

public class ProgressInfo {
    public static final String ACTIVE_BRANCH_TAG = "branch";
    public static final String ACTIVE_BRANCH_ID_ATTR = "id";
    public static final String BOOK_MARK_TAG = "bookMark";
    public static final String BOOK_MARK_SECTION_ID_ATTR = "sectionId";
    public static final String BOOK_MARK_LOCATION_ATTR = "location";

    private final String branchId;
    private final BookMark bookMark;

    public ProgressInfo(String branchId, BookMark bookMark) {
        this.branchId = branchId;
        this.bookMark = bookMark;
    }

    private static boolean validateInteger(String loc) {
        Scanner scanner = new Scanner(loc);
        return scanner.hasNextInt() &&
                !scanner.hasNext();
    }

    public static ProgressInfo fromFile(File file) throws Exception {
        InputStream in = null;
        try {
            if (!IOUtils.fileExists(file)) {
                throw new FileNotFoundException(
                        String.format("specified file %s doesn't match any exists one", file.getAbsolutePath()));
            }
            in = new FileInputStream(file);
            XmlPullParser xpp = XmlPullParserFactory.newInstance().newPullParser();
            xpp.setInput(in, "utf-8");

            String br = null;
            BookMark bm = null;

            for (; xpp.next() != XmlPullParser.END_DOCUMENT; ) {
                if (xpp.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                if (xpp.getName().equals(ACTIVE_BRANCH_TAG)) {
                    br = xpp.getAttributeValue(null, ACTIVE_BRANCH_ID_ATTR);
                } else if (xpp.getName().equals(BOOK_MARK_TAG)) {
                    String sectionId = xpp.getAttributeValue(null, BOOK_MARK_SECTION_ID_ATTR);
                    String locStr = xpp.getAttributeValue(null, BOOK_MARK_LOCATION_ATTR);
                    int loc = 0;
                    if (validateInteger(locStr)) {
                        loc = Math.max(0, Integer.parseInt(locStr));
                    }
                    bm = new BookMark(sectionId, loc);
                }
            }
            return new ProgressInfo(br, bm);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    public BookMark getBookMark() {
        return bookMark;
    }

    public String getBranchId() {
        return branchId;
    }

    public String toXmlString() {
        return String.format(Locale.US, "<%s %s=\"%s\"/>%n<%s %s=\"%s\" %s=\"%d\"/>",
                ACTIVE_BRANCH_TAG, ACTIVE_BRANCH_ID_ATTR, branchId,
                BOOK_MARK_TAG, BOOK_MARK_SECTION_ID_ATTR, bookMark.getSectionId(),
                BOOK_MARK_LOCATION_ATTR, bookMark.getLocation());
    }
}