package com.gitlab.yatxt.exc;

public class XmlParsingException extends YaTxtXmlTokenizerException {
    public XmlParsingException(String message) {
        super(message);
    }
}
