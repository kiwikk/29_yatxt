package com.gitlab.yatxt.book;

public class BookMark {
    private final String sectionId;
    private final int location;

    public BookMark(String sectionId, int location) {
        this.sectionId = sectionId;
        this.location = location;
    }

    public int getLocation() {
        return location;
    }

    public String getSectionId() {
        return sectionId;
    }
}