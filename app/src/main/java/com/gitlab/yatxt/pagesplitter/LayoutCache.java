package com.gitlab.yatxt.pagesplitter;

import android.text.StaticLayout;
import android.util.LruCache;

import static com.gitlab.yatxt.pagesplitter.PuzzleChunkSupplier.PuzzleChunk;

/**
 * This class thread-safe, as it only uses internally synchronized lru cache.
 */
public class LayoutCache {
    private LruCache<PuzzleChunk, StaticLayout> cache;

    private static final int DEFAULT_CACHE_SIZE = 10;
    private static final LayoutCache singleton = new LayoutCache(DEFAULT_CACHE_SIZE);

    private LayoutCache(int cacheSize) {
        this.cache = new LruCache<>(cacheSize);
    }

    public static LayoutCache get() {
        return singleton;
    }

    public void put(PuzzleChunk chunk, StaticLayout layout) {
        if (cache.get(chunk) != null) {
            throw new RuntimeException("attempt to replace existing key");
        }
        cache.put(chunk, layout);
    }

    public StaticLayout get(PuzzleChunk chunk) {
        return cache.get(chunk);
    }

    public void clear() {
        cache.evictAll();
    }
}
