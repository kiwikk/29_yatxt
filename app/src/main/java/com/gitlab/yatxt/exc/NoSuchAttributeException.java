package com.gitlab.yatxt.exc;

public class NoSuchAttributeException extends YaTxtXmlTokenizerException {
    public NoSuchAttributeException(String message) {
        super(message);
    }
}
