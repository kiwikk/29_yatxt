package com.gitlab.yatxt.pagesplitter;

import android.text.StaticLayout;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.yatxt.R;
import com.gitlab.yatxt.ui.views.TextLayoutView;

import java.util.ArrayList;
import java.util.HashMap;

import static com.gitlab.yatxt.pagesplitter.PuzzleChunkSupplier.PuzzleChunk;

public abstract class PuzzleChunkAdapter extends RecyclerView.Adapter<PuzzleChunkAdapter.TextLayoutViewHolder> {
    private final ArrayList<PuzzleChunk> chunks;
    private final LayoutCache layoutCache = LayoutCache.get();
    private final HashMap<PuzzleChunk, Animation> animatedItems = new HashMap<>();

    public PuzzleChunkAdapter(ArrayList<PuzzleChunk> chunks) {
        this.chunks = chunks;
    }

    public void addAnimatedItem(PuzzleChunk chunk, Animation animation) {
        animatedItems.put(chunk, animation);
    }

    @NonNull
    @Override
    public TextLayoutViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextLayoutView view = (TextLayoutView) LayoutInflater.from(parent.getContext()).
                inflate(R.layout.text_layout_view, parent, false);
        return new TextLayoutViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TextLayoutViewHolder holder, int position) {
        PuzzleChunk chunk = chunks.get(position);
        if (animatedItems.containsKey(chunk)) {
            holder.view.setAnimation(animatedItems.get(chunk));
            animatedItems.remove(chunk);
        }
        holder.bind(position);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull TextLayoutViewHolder holder) {
        holder.view.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return chunks.size();
    }

    class TextLayoutViewHolder extends RecyclerView.ViewHolder {
        TextLayoutView view;

        TextLayoutViewHolder(@NonNull TextLayoutView itemView) {
            super(itemView);
            this.view = itemView;
        }

        void bind(int position) {
            PuzzleChunk chunk = chunks.get(position);
            StaticLayout layout = layoutCache.get(chunk);
            if (layout != null) {
                view.setLayout(layout);
            } else {
                onLayoutRequested(position);
            }
        }
    }

    public abstract void onLayoutRequested(int position);
}
