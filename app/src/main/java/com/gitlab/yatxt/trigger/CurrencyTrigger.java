package com.gitlab.yatxt.trigger;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class CurrencyTrigger extends Trigger {
    private final ExecutorService worker = Executors.newFixedThreadPool(1);

    CurrencyTrigger(Context context) {
        super(context);
    }

    @Override
    public void addSwitch(String condition, String sectionId) {
        switches.add(new CurrencySwitch(condition, sectionId));
    }

    @Override
    public void evaluate() {
        worker.submit(new CurrencyRequestTask());
    }

    private class CurrencyRequestTask implements Runnable {
        CurrencyCourse currencyCourse;
        private final String currencyCourseKey = "currencycourse";

        Handler handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(@NonNull Message msg) {
                Bundle data = msg.getData();
                String result = data.getString(currencyCourseKey);
                sendResult(result);
                return false;
            }
        });

        private void sendErrResult() {
            Bundle data = new Bundle();
            data.putString(currencyCourseKey, "err");
            Message msg = new Message();
            msg.setData(data);
            handler.sendMessage(msg);
        }

        String getCurrentDate() {
            Calendar calendar = Calendar.getInstance();
            String day = Integer.valueOf(calendar.get(Calendar.DAY_OF_MONTH)).toString();
            if (day.length() != 2) {
                day = "0" + day;
            }
            String month = Integer.valueOf(calendar.get(Calendar.MONTH) + 1).toString();
            if (month.length() != 2) {
                month = "0" + month;
            }
            String year = Integer.valueOf(calendar.get(Calendar.YEAR)).toString();

            return day + "/" + month + "/" + year;
        }

        private void parseXML(HttpURLConnection connection) {
            try {
                currencyCourse = new CurrencyCourse();

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                InputStream inputStream = connection.getInputStream();
                Document document = builder.parse(connection.getInputStream());

                NodeList charCodeElements = document.getDocumentElement().getElementsByTagName("CharCode");
                NodeList valueElements = document.getDocumentElement().getElementsByTagName("Value");

                for (int i = 0; i < charCodeElements.getLength(); ++i) {
                    NodeList charCodeList = charCodeElements.item(i).getChildNodes();
                    NodeList valueList = valueElements.item(i).getChildNodes();

                    currencyCourse.addCurrency(new Currency(charCodeList.item(0).getNodeValue(),
                            valueList.item(0).getNodeValue()));
                }
            } catch (Throwable ignored) {
                sendErrResult();
                return;
            }

            Bundle data = new Bundle();
            String str = currencyCourse.toString();
            data.putString(currencyCourseKey, currencyCourse.toString());
            Message msg = new Message();
            msg.setData(data);
            handler.sendMessage(msg);
        }

        @Override
        public void run() {
            URL url = null;
            try {
                String urlString = "http://www.cbr.ru/scripts/XML_daily.asp?date_req=" + getCurrentDate();
                url = new URL(urlString);
            } catch (MalformedURLException ignored) {
                sendErrResult();
                return;
            }

            try {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                parseXML(connection);
            } catch (IOException ignored) {
                sendErrResult();
            }
        }
    }

    private class CurrencySwitch extends Trigger.Switch {
        CurrencySwitch(String condition, String sectionId) {
            super(condition, sectionId);
        }

        @Override
        public boolean matchCondition(String value) {
            if (condition.equals(DEFAULT_CONDITION)) {
                return true;
            }
            if (value.equals("err")) {
                return false;
            }

            try {
                String[] valueArr = condition.split(" ");
                double doubleValue = Double.parseDouble(valueArr[1]);
                String charCode = valueArr[0];

                valueArr = value.split(" ");
                for (int i = 0; i < valueArr.length - 1; ++i) {
                    if (valueArr[i].toLowerCase().equals(charCode)) {
                        double realValue = Double.parseDouble(valueArr[i + 1]);
                        return (realValue <= doubleValue);
                    }
                }

                return false;
            } catch (Throwable ignored) {
                return false;
            }
        }
    }

    private class Currency {
        String charCode;
        double value;

        Currency(String charCode, String value) {
            this.charCode = charCode;
            this.value = Double.parseDouble(value.replace(',', '.'));
        }

        Currency(String charCode, double value) {
            this.charCode = charCode;
            this.value = value;
        }

        String getCharCode() {
            return charCode;
        }

        double getValue() {
            return value;
        }
    }

    private class CurrencyCourse {
        ArrayList<Currency> currencies;

        CurrencyCourse() {
            currencies = new ArrayList<>();
        }

        void addCurrency(Currency currency) {
            currencies.add(currency);
        }

        Currency getCurrency(int index) {
            return currencies.get(index);
        }

        double getCurrencyValue(String charCode) {
            for (int i = 0; i < currencies.size(); ++i) {
                if (currencies.get(i).getCharCode().equals(charCode)) {
                    return currencies.get(i).getValue();
                }
            }

            return -1;
        }

        @Override
        public String toString() {
            String stringForm = "";
            for (Currency currency : currencies) {
                stringForm += currency.charCode + " " + currency.value + " ";
            }

            return stringForm;
        }
    }
}
