package com.gitlab.yatxt.ui.library;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.yatxt.R;
import com.gitlab.yatxt.ui.library.description.DescriptionActivity;
import com.gitlab.yatxt.ui.library.description.ItemClickListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LibraryAdapter extends RecyclerView.Adapter<LibraryHolder> implements Filterable {

    Context c;
    ArrayList<BookModel> bookModels;
    ArrayList<BookModel> bookCopies;

    public LibraryAdapter(Context c, ArrayList<BookModel> bookModels) {
        this.c = c;
        this.bookModels = bookModels;
        bookCopies = new ArrayList<>(bookModels);
    }

    @NonNull
    @Override
    public LibraryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row, null);
        return new LibraryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LibraryHolder libraryHolder, int i) {
        libraryHolder.mtitle.setText(bookModels.get(i).getTitle());
        libraryHolder.mAuth.setText(bookModels.get(i).getAuthor());
        new OkHttpImageHandler(libraryHolder.imageView).execute(bookModels.get(i).getUrl());

        libraryHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void OnItemClickListener(View v, int position) {
                Log.d("LibraryAdapter", "in setItemClickListener 45");
                Intent intent = new Intent(c, DescriptionActivity.class);
                intent.putExtra("bookName", bookModels.get(position).getBookName());
                intent.putExtra("iTitle", bookModels.get(position).getTitle());
                intent.putExtra("iAuthor", bookModels.get(position).getAuthor());
                intent.putExtra("iGenre", bookModels.get(position).getGenre());
                intent.putExtra("iDescription", bookModels.get(position).getDescription());
                intent.putExtra("ID", bookModels.get(position).getId());

                DescriptionActivity.URL = bookModels.get(position).getUrl();
                c.startActivity(intent);
            }
        });
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<BookModel> result = new ArrayList<>();
            String text = constraint.toString();

            if (text.isEmpty()) {
                result.addAll(bookModels);
            } else {
                for (BookModel book :
                        bookModels
                ) {
                    if (book.getTitle().toLowerCase().contains(text)
                            || book.getDescription().toLowerCase().contains(text)) {
                        result.add(book);
                    }
                }
                bookModels.clear();
                bookModels.addAll(result);
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = result;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            bookModels.clear();
            bookModels.addAll((Collection<? extends BookModel>) results.values);

            notifyDataSetChanged();
        }
    };

    @Override
    public int getItemCount() {
        return bookModels.size();
    }


//    public void filter(String text) {
//        if (text.isEmpty()) {
//            bookModels.clear();
//        } else {
//            ArrayList<BookModel> result = new ArrayList<>();
//            text = text.toLowerCase();
//
//            for (BookModel book :
//                    bookModels
//            ) {
//                if (book.getTitle().toLowerCase().contains(text)
//                        || book.getDescription().toLowerCase().contains(text)) {
//                    result.add(book);
//                }
//            }
//            bookModels.clear();
//            bookModels.addAll(result);
//        }
//        notifyDataSetChanged();
//    }
}
