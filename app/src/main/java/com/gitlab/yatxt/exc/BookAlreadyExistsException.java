package com.gitlab.yatxt.exc;

public class BookAlreadyExistsException extends RuntimeException {
    public BookAlreadyExistsException() {
        super();
    }

    public BookAlreadyExistsException(String msg) {
        super(msg);
    }
}
