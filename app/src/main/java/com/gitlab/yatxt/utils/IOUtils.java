package com.gitlab.yatxt.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;

public class IOUtils {
    private IOUtils() {
    }

    public static void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }

    public static boolean fileExists(File file) {
        return file.exists() && file.isFile();
    }

    public static boolean dirExists(File file) {
        return file.exists() && file.isDirectory();
    }

    public static File getChild(File dir, final String name) {
        return new File(dir.getPath(), name);
    }

    public static int read(RandomAccessFile file, byte[] b, int arrOffset,
                           int len, int fileOffset) throws IOException {
        file.seek(fileOffset);
        return file.read(b, arrOffset, len);
    }

    public static CharSequence readChars(RandomAccessFile file, int len, int fileOffset) throws IOException {
        byte[] b = new byte[len];
        int bytesRead = read(file, b, 0, len, fileOffset);
        return (bytesRead < 1) ? ("") : (new String(b, 0, bytesRead, StandardCharsets.UTF_8));
    }

    public static boolean dirEmpty(File dir) {
        File[] files = dir.listFiles();
        return files == null || files.length == 0;
    }

    public static void deleteIfExists(File file) throws IOException {
        if (fileExists(file)) {
            FileUtils.forceDelete(file);
        }
    }

    public static void forceCreate(File file) throws IOException {
        if (!file.createNewFile()) {
            throw new IOException("failed while creating file: " + file.getAbsolutePath());
        }
    }

    public static boolean forceMkdirIfNotExists(File dir) throws IOException {
        boolean exists = IOUtils.dirExists(dir);
        if (!exists) {
            if (!dir.mkdir()) {
                throw new IOException("failed while creating directory: " + dir.getAbsolutePath());
            }
        }
        return !exists;
    }
}
