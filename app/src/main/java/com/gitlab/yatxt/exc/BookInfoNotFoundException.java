package com.gitlab.yatxt.exc;

import java.io.IOException;

public class BookInfoNotFoundException extends IOException {
    public BookInfoNotFoundException() {
        super();
    }

    public BookInfoNotFoundException(String message) {
        super(message);
    }
}
