package com.gitlab.yatxt.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.yatxt.R;
import com.gitlab.yatxt.YaTxtApplication;
import com.gitlab.yatxt.book.Book;

import java.io.File;
import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    RecyclerView rv;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        rv = root.findViewById(R.id.read_recycle);
        File folder = YaTxtApplication.getAppDir();
        File[] files = folder.listFiles();

        if (files != null) {
            ArrayList<String> book_names = new ArrayList<>();
            for (File f : files
            ) {
                book_names.add(f.getPath());
            }

            ArrayList<Book> books = new ArrayList<>();

            for (String path : book_names) {
                try {
                    books.add(Book.fromRootDir(path));
                } catch (Exception e) {
                    Log.e("HomeFragment", "line 49");
                    e.printStackTrace();
                }
            }

            BookRecycleAdapter adapter = new BookRecycleAdapter(books);
            rv.setAdapter(adapter);
        }

        return root;
    }


}