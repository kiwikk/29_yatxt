package com.gitlab.yatxt.ui.library.description;

import android.view.View;

public interface ItemClickListener {

    void OnItemClickListener(View v, int position);
}
