package com.gitlab.yatxt.ui.library.description;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.gitlab.yatxt.R;
import com.gitlab.yatxt.YaTxtApplication;
import com.gitlab.yatxt.book.Book;
import com.gitlab.yatxt.book.BooksObserver;
import com.gitlab.yatxt.ui.ReadActivity;
import com.gitlab.yatxt.ui.library.OkHttpImageHandler;
import com.gitlab.yatxt.utils.ApiUtils;

import java.util.LinkedList;
import java.util.List;

public class DescriptionActivity extends AppCompatActivity {

    public static String URL;

    TextView title;
    TextView author;
    TextView genre;
    TextView description;

    ImageView imageView;

    Button readButton;

    String bookId;
    String bookName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");

        title = findViewById(R.id.book_name);
        author = findViewById(R.id.author);
        genre = findViewById(R.id.genre);
        description = findViewById(R.id.description);
        imageView = findViewById(R.id.imageView);
        readButton = findViewById(R.id.button);

        readButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReadClicked();
            }
        });

        Intent intent = getIntent();

        title.setText(intent.getStringExtra("iTitle"));
        author.setText(intent.getStringExtra("iAuthor"));
        genre.setText(intent.getStringExtra("iGenre"));
        description.setText(intent.getStringExtra("iDescription"));

        bookId = intent.getStringExtra("ID");
        bookName = intent.getStringExtra("bookName");

        new OkHttpImageHandler(imageView).execute(URL);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.rgb(242, 242, 242)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.description_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                //TODO: add sth
                //https://www.journaldev.com/9357/android-actionbar-example-tutorial
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void onReadClicked() {
        Log.d(YaTxtApplication.TAG, "read button clicked");
        tryRead();
    }

    void tryRead() {
        BooksObserver booksObserver = BooksObserver.getInstance();
        Book book = booksObserver.getBookById(bookId);
        if (book != null) {
            ReadActivity.startReadActivity(this, book.getRootDir());
        } else {
            Log.d(YaTxtApplication.TAG, String.format("start load book %s from server", bookId));
            ApiUtils.getInstance().requestBookDownload(this, bookName, new Runnable() {
                @Override
                public void run() {
                    onDownloadComplete();
                }
            });
        }
    }

    void onDownloadComplete() {
        Log.d(YaTxtApplication.TAG, "download complete");
        BooksObserver.getInstance().refresh();
    }

    public String getBookId() {
        return bookId;
    }
}
