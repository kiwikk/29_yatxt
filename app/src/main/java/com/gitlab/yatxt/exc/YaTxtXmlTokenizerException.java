package com.gitlab.yatxt.exc;

public class YaTxtXmlTokenizerException extends RuntimeException {
    public YaTxtXmlTokenizerException(String message) {
        super("YaTxtXmlTokenizer: " + message);
    }
}
