package com.gitlab.yatxt.trigger;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import java.util.Arrays;

public class PressureTrigger extends Trigger {
    private SensorManager sensorManager;

    private final String pressureKey = "pressure";
    Handler handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            Bundle data = msg.getData();
            String result = data.getString(pressureKey);
            sendResult(result);
            return false;
        }
    });

    private void sendErrResult() {
        Bundle data = new Bundle();
        data.putString(pressureKey, "err");
        Message msg = new Message();
        msg.setData(data);
        handler.sendMessage(msg);
    }

    PressureTrigger(Context context) {
        super(context);
    }

    @Override
    public void addSwitch(String condition, String sectionId) {
        switches.add(new PressureSwitch(condition, sectionId));
    }

    @Override
    public void evaluate() {
        try {
            sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
            Sensor pressureSensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
            if (pressureSensor == null) {
                sendErrResult();
                return;
            }

            sensorManager.registerListener(this.new PressureListener(), pressureSensor, SensorManager.SENSOR_DELAY_NORMAL);
        } catch (Throwable ignored) {
            sendErrResult();
        }
    }

    private class PressureListener implements SensorEventListener {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            Sensor triggeredSensor = sensorEvent.sensor;

            if (triggeredSensor.getType() == Sensor.TYPE_PRESSURE) {
                sensorManager.unregisterListener(this);
                sendResult(Double.toString(sensorEvent.values[0]));
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    }

    private class PressureSwitch extends Trigger.Switch {
        PressureSwitch(String condition, String sectionId) {
            super(condition, sectionId);
        }

        @Override
        public boolean matchCondition(String value) {
            if (condition.equals(DEFAULT_CONDITION)) {
                return true;
            }
            if (value.equals("err")) {
                return false;
            }

            try {
                return Double.parseDouble(value) <= Double.parseDouble(condition);
            } catch (Throwable ignored) {
                return false;
            }
        }
    }
}
