package com.gitlab.yatxt.trigger;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.gitlab.yatxt.trigger.openweather.OpenWeather;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WeatherTrigger extends Trigger {
    private LocationManager locationManager;
    private final ExecutorService worker = Executors.newFixedThreadPool(1);
    double latitude;
    double longitude;

    private final String weatherKey = "weather";
    Handler handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            Bundle data = msg.getData();
            String result = data.getString(weatherKey);
            sendResult(result);
            return false;
        }
    });

    private void sendErrResult() {
        Bundle data = new Bundle();
        data.putString(weatherKey, "err");
        Message msg = new Message();
        msg.setData(data);
        handler.sendMessage(msg);
    }

    WeatherTrigger(Context context) {
        super(context);
    }

    @Override
    public void addSwitch(String condition, String sectionId) {
        switches.add(new WeatherSwitch(condition, sectionId));
    }

    @Override
    public void evaluate() {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        0, 1, this.new GPSListener());

                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location == null) {
                    sendErrResult();
                }
            }
        } catch (Throwable ignored) {
            sendErrResult();
        }

    }

    private class WeatherRequestTask implements Runnable {
        OpenWeather weather;
        private final String apiKey = "53f694bcee2cc0e57a0fd3b732465770";

        @Override
        public void run() {
            URL url = null;
            try {
                String urlString = "https://api.openweathermap.org/data/2.5/weather?lat=" +
                        latitude + "&lon=" + longitude + "&appid=" + apiKey;
                url = new URL(urlString);
            } catch (MalformedURLException ignored) {
                sendErrResult();
                return;
            }

            try {
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                    weather = new Gson().fromJson(in.readLine(), OpenWeather.class);

                    Bundle data = new Bundle();
                    data.putString(weatherKey, Double.valueOf(weather.main.temp).toString());
                    Message msg = new Message();
                    msg.setData(data);
                    handler.sendMessage(msg);
                } catch (Throwable ignored) {
                    sendErrResult();
                }
            } catch (IOException ignored) {
                sendErrResult();
            }
        }
    }

    private class GPSListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            worker.submit(new WeatherRequestTask());
            locationManager.removeUpdates(this);
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    private class WeatherSwitch extends Switch {
        WeatherSwitch(String condition, String sectionId) {
            super(condition, sectionId);
        }

        @Override
        public boolean matchCondition(String value) {
            if (condition.equals(DEFAULT_CONDITION)) {
                return true;
            }
            if (value.equals("err")) {
                return false;
            }

            double realCondition;
            double realValue;
            try {
                realCondition = Double.parseDouble(condition);
                realValue = Double.parseDouble(value) - 273;
            } catch (Throwable ignored) {
                return false;
            }
            return (realValue <= realCondition);
        }
    }
}
