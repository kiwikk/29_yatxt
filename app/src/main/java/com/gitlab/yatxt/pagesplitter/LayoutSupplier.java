package com.gitlab.yatxt.pagesplitter;

import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;

public class LayoutSupplier {
    private final float width;
    private final TextPaint paint;
    private final StaticLayout.Alignment alignment;
    private final float spacingAdd;
    private final float spacingMultiplier;
    private final boolean includePadding;

    public LayoutSupplier(Layout template) {
        this.width = template.getWidth();
        this.paint = template.getPaint();
        this.alignment = template.getAlignment();
        this.spacingAdd = template.getSpacingAdd();
        this.spacingMultiplier = template.getSpacingMultiplier();
        this.includePadding = true;
    }

    public StaticLayout obtain(SpannableStringBuilder text) {
        return new StaticLayout(text, paint, (int) width,
                alignment, spacingMultiplier, spacingAdd, includePadding);
    }

    public TextPaint getPaint() {
        return paint;
    }

    public float getWidth() {
        return width;
    }
}
